package main

import (
	"fmt"

	"advent-of-code-2019/pkg/dayXX"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("X: %d\n", dayXX.X(input))
	fmt.Printf("Y: %d\n", dayXX.Y(input))
}
