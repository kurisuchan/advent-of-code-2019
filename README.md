This repository is a collection of my solutions for the [Advent of Code 2019](http://adventofcode.com/2019) calendar.

Puzzle                                                    | Silver                | Gold
--------------------------------------------------------- | --------------------- | ---------------------
[Day 1: The Tyranny of the Rocket Equation](pkg/day01)    | `00:15:53`   (`1665`) | `00:21:39`   (`1318`)
[Day 2: 1202 Program Alarm](pkg/day02)                    | `00:16:46`   (`1189`) | `00:24:53`   (` 958`)
[Day 3: Crossed Wires](pkg/day03)                         | `00:38:45`   (`1327`) | `00:47:10`   (`1033`)
[Day 4: Secure Container](pkg/day04)                      | `00:30:28`   (`3347`) | `00:44:11`   (`2694`)
[Day 5: Sunny with a Chance of Asteroids](pkg/day05)      | `00:45:39`   (`1253`) | `00:59:59`   (`1049`)
[Day 6: Universal Orbit Map](pkg/day06)                   | `00:16:12`   (` 806`) | `00:27:41`   (` 702`)
[Day 7: Amplification Circuit](pkg/day07)                 | `00:22:50`   (` 726`) | `00:42:33`   (` 238`)
[Day 8: Space Image Format](pkg/day08)                    | `00:16:16`   (`1206`) | `00:37:23`   (`1335`)
[Day 9: Sensor Boost](pkg/day09)                          | `00:45:56`   (` 947`) | `00:47:06`   (` 916`)
[Day 10: Monitoring Station](pkg/day10)                   | `00:18:52`   (` 206`) | `00:45:54`   (` 120`)
[Day 11: Space Police](pkg/day11)                         | `00:16:18`   (` 252`) | `00:22:00`   (` 233`)
[Day 12: The N-Body Problem](pkg/day12)                   | `00:36:50`   (`1218`) | `00:57:03`   (` 377`)
[Day 13: Care Package](pkg/day13)                         | `00:06:36`   (` 508`) | `00:27:02`   (` 232`)
[Day 14: Space Stoichiometry](pkg/day14)                  | `01:33:42`   (` 790`) | `02:10:22`   (` 724`)
[Day 15: Oxygen System](pkg/day15)                        | `01:34:54`   (` 710`) | `02:32:34`   (` 841`)
[Day 16: Flawed Frequency Transmission](pkg/day16)        | `00:41:24`   (`1128`) | `01:33:07`   (` 276`)
[Day 17: Set and Forget](pkg/day17)                       | `00:20:48`   (` 723`) | `01:04:24`   (` 269`)
[Day 18: Many-Worlds Interpretation](pkg/day18)           | `    >24h`   (`2804`) | `    >24h`   (`2279`)
[Day 19: Tractor Beam](pkg/day19)                         | `00:05:14`   (` 210`) | `00:55:58`   (` 423`)
[Day 20: Donut Maze](pkg/day20)                           | `00:41:09`   (` 239`) | `02:45:59`   (` 539`)
[Day 21: Springdroid Adventure](pkg/day21)                | `00:47:28`   (` 578`) | `01:51:54`   (` 603`)
[Day 22: Slam Shuffle](pkg/day22)                         | `00:45:21`   (` 649`) | `03:57:00`   (` 360`)[^1]
[Day 23: Category Six](pkg/day23)                         | `00:36:01`   (` 453`) | `01:47:59`   (` 727`)
[Day 24: Planet of Discord](pkg/day24)                    | `00:27:28`   (` 499`) | `01:09:47`   (` 319`)
[Day 25: Cryostasis](pkg/day25)                           | `00:28:13`   (`  80`) | `00:28:46`   (`  69`)


[^1]: Part 2 credit goes to the mathemagicians on reddit. I just reimplemented their incantations as I am not a
      participant in Advent of Math.
