package day18

import (
	"advent-of-code-2019/pkg/utils"
)

func OneVault(input []string) int {
	starts := ParseMap(input, false)
	start := starts[0]

	var allKeys uint32
	for _, n := range start.Neighbors {
		allKeys |= n.Keys
	}

	cstart := Cache{Node: start}
	frontier := utils.NewQueue()
	frontier.Set(cstart, 0)

	for !frontier.IsEmpty() {
		p, distance := frontier.Pop()
		pos := p.(Cache)

		if pos.Keys == allKeys {
			return distance
		}

		for _, neighbor := range pos.Node.Neighbors {
			nc := Cache{Node: neighbor.Target, Keys: pos.Keys}

			if pos.Keys&neighbor.Doors != neighbor.Doors || pos.Keys&neighbor.Keys == neighbor.Keys {
				continue
			}

			nc.Keys |= neighbor.Keys

			if cost, ok := frontier.Check(nc); ok {
				if distance+neighbor.Distance < cost {
					frontier.Set(nc, distance+neighbor.Distance)
				}
			} else {
				frontier.Set(nc, distance+neighbor.Distance)
			}
		}
	}
	return -1
}

func QuadVault(input []string) int {
	starts := ParseMap(input, true)

	var allKeys uint32

	cstart := Cache4{}

	for i, start := range starts {
		cstart.Nodes[i] = start
		for _, n := range start.Neighbors {
			allKeys |= n.Keys
		}
	}

	frontier := utils.NewQueue()
	frontier.Set(cstart, 0)

	for !frontier.IsEmpty() {
		p, distance := frontier.Pop()
		pos := p.(Cache4)

		if pos.Keys == allKeys {
			return distance
		}

		for i, robot := range pos.Nodes {
			for _, neighbor := range robot.Neighbors {
				nc := Cache4{Nodes: pos.Nodes, Keys: pos.Keys}
				nc.Nodes[i] = neighbor.Target

				if pos.Keys&neighbor.Doors != neighbor.Doors || pos.Keys&neighbor.Keys == neighbor.Keys {
					continue
				}

				nc.Keys |= neighbor.Keys

				if cost, ok := frontier.Check(nc); ok {
					if distance+neighbor.Distance < cost {
						frontier.Set(nc, distance+neighbor.Distance)
					}
				} else {
					frontier.Set(nc, distance+neighbor.Distance)
				}
			}
		}
	}

	return -1
}

type Node struct {
	Name      rune
	Neighbors []*Edge
}

type Edge struct {
	Doors, Keys uint32
	Distance    int
	Target      *Node
}

type Cache struct {
	Node *Node
	Keys uint32
}


type Cache4 struct {
	Nodes  [4]*Node
	Keys  uint32
}

func ParseMap(input []string, part2 bool) []*Node {
	grid := make(map[utils.Coordinate]*Node)
	keys := make(map[rune]utils.Coordinate)
	var startPos utils.Coordinate
	for y, row := range input {
		for x, r := range row {
			if r == '#' {
				continue
			}
			pos := utils.Coordinate{X: x, Y: y}
			if r == '@' {
				startPos = pos
			}
			if r >= 'a' && r <= 'z' {
				keys[r] = pos
			}
			grid[pos] = &Node{Name: r}
		}
	}

	var starts []utils.Coordinate
	if part2 {
		for _, d := range utils.Diagonals {
			starts = append(starts, startPos.Move(d))
		}
		delete(grid, startPos)
		for _, d := range utils.Cardinals {
			delete(grid, startPos.Move(d))
		}
	} else {
		starts = append(starts, startPos)
	}

	var startNodes []*Node
	for _, s := range starts {
		start := &Node{Name: '@'}
		grid[s] = start
		for r, c := range keys {
			node := grid[c]
			edge := Path(s, c, grid)
			if edge != nil {
				edge.Target = node
				start.Neighbors = append(start.Neighbors, edge)
			}

			for rb, rc := range keys {
				if rb == r {
					continue
				}
				target := grid[rc]
				edge := Path(c, rc, grid)
				if edge != nil {
					edge.Target = target
					node.Neighbors = append(node.Neighbors, edge)
				}
			}
		}
		startNodes = append(startNodes, start)
	}
	return startNodes
}

func Path(start, target utils.Coordinate, grid map[utils.Coordinate]*Node) *Edge {
	frontier := utils.NewQueue()
	frontier.Set(start, 0)
	visited := map[utils.Coordinate]bool{
		start: true,
	}

	previous := make(map[utils.Coordinate]utils.Coordinate)

	for !frontier.IsEmpty() {
		p, distance := frontier.Pop()
		pos := p.(utils.Coordinate)

		if pos == target {
			edge := &Edge{Distance: distance}
			for pos != start {
				switch kd, mask := KeyDoor(grid[pos].Name); kd {
				case Door:
					edge.Doors |= mask
				case Key:
					edge.Keys |= mask
				}
				pos = previous[pos]
			}

			return edge
		}

		for _, d := range utils.Cardinals {
			neighbor := pos.Move(d)

			if _, ok := grid[neighbor]; !ok {
				continue
			}

			if visited[neighbor] {
				continue
			}
			visited[neighbor] = true

			if cost, ok := frontier.Check(neighbor); ok {
				if distance+1 < cost {
					frontier.Set(neighbor, distance+1)
					previous[neighbor] = pos
				}
			} else {
				frontier.Set(neighbor, distance+1)
				previous[neighbor] = pos
			}
		}
	}
	return nil
}

const (
	Key uint8 = iota
	Door
)

func KeyDoor(r rune) (uint8, uint32) {
	if r >= 'A' && r <= 'Z' {
		return Door, 1 << uint(r-'Z'+25)
	}
	if r >= 'a' && r <= 'z' {
		return Key, 1 << uint(r-'z'+25)
	}
	return 0, 0
}
