package day18_test

import (
	"testing"

	"advent-of-code-2019/pkg/day18"
)

func TestOneVault(t *testing.T) {
	tests := []struct {
		in   []string
		out  int
		long bool
	}{
		{
			[]string{
				"#########",
				"#b.A.@.a#",
				"#########",
			}, 8, false},
		{[]string{
			"########################",
			"#f.D.E.e.C.b.A.@.a.B.c.#",
			"######################.#",
			"#d.....................#",
			"########################",
		}, 86, false},
		{[]string{
			"########################",
			"#...............b.C.D.f#",
			"#.######################",
			"#.....@.a.B.c.d.A.e.F.g#",
			"########################",
		}, 132, false},
		{[]string{
			"#################",
			"#i.G..c...e..H.p#",
			"########.########",
			"#j.A..b...f..D.o#",
			"########@########",
			"#k.E..a...g..B.n#",
			"########.########",
			"#l.F..d...h..C.m#",
			"#################",
		}, 136, true},
		{[]string{
			"########################",
			"#@..............ac.GI.b#",
			"###d#e#f################",
			"###A#B#C################",
			"###g#h#i################",
			"########################",
		}, 81, false},
	}

	for i, test := range tests {
		if testing.Short() && test.long {
			t.Logf("skipped long test #%d", i)
			continue
		}
		actual := day18.OneVault(test.in)
		if actual != test.out {
			t.Errorf("OneVault(#%d) => %d, want %d", i, actual, test.out)
		}

	}
}

func TestQuadVault(t *testing.T) {
	tests := []struct {
		in   []string
		out  int
		long bool
	}{
		{[]string{
			"#######",
			"#a.#Cd#",
			"##...##",
			"##.@.##",
			"##...##",
			"#cB#Ab#",
			"#######",
		}, 8, false},
		{[]string{
			"###############",
			"#d.ABC.#.....a#",
			"######...######",
			"######.@.######",
			"######...######",
			"#b.....#.....c#",
			"###############",
		}, 24, false},
		{[]string{
			"#############",
			"#DcBa.#.GhKl#",
			"#.###...#I###",
			"#e#d#.@.#j#k#",
			"###C#...###J#",
			"#fEbA.#.FgHi#",
			"#############",
		}, 32, false},
		{[]string{
			"#############",
			"#g#f.D#..h#l#",
			"#F###e#E###.#",
			"#dCba...BcIJ#",
			"#####.@.#####",
			"#nK.L...G...#",
			"#M###N#H###.#",
			"#o#m..#i#jk.#",
			"#############",
		}, 72, false},
	}

	for i, test := range tests {
		if testing.Short() && test.long {
			t.Logf("skipped long test #%d", i)
			continue
		}
		actual := day18.QuadVault(test.in)
		if actual != test.out {
			t.Errorf("QuadVault(#%d) => %d, want %d", i, actual, test.out)
		}
	}
}
