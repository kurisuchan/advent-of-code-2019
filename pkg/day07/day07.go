package day07

import (
	"fmt"
	"sync"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day05"
	"advent-of-code-2019/pkg/utils"
)

// #goroutines #concurrency #channels

func Part1(input string) int {
	intCode := day02.ParseIntcode(input)
	var max int
	var wg sync.WaitGroup
	var lock sync.RWMutex

	for perm := range utils.Permutations(0, 4) {
		in, out := Amps(perm, intCode)
		wg.Add(1)

		go func() {
			result := <-out

			lock.Lock()
			if result > max {
				max = result
			}
			lock.Unlock()

			wg.Done()
		}()

		in <- 0
	}

	wg.Wait()
	return max
}

func Part2(input string) int {
	intCode := day02.ParseIntcode(input)
	var max int
	var wg sync.WaitGroup
	var lock sync.RWMutex

	for perm := range utils.Permutations(5, 9) {
		in, out := Amps(perm, intCode)
		wg.Add(1)

		go func() {
			var result int

			for result = range out {
				go func() { in <- result }()
			}

			lock.Lock()
			if result > max {
				max = result
			}
			lock.Unlock()

			wg.Done()
		}()

		in <- 0
	}

	wg.Wait()
	return max
}

func Program(memory []int, in, out chan int) {
	var pos, mask, output int

	for {
		mask = memory[pos] / 100

		switch memory[pos] % 100 {
		case 1:
			pos = day05.InstructionAdd(memory, pos, mask)
		case 2:
			pos = day05.InstructionMultiply(memory, pos, mask)
		case 3:
			pos = day05.InstructionInput(memory, pos, mask, <-in)
		case 4:
			pos, output = day05.InstructionOutput(memory, pos, mask)
			out <- output
		case 5:
			pos = day05.InstructionJumpIfTrue(memory, pos, mask)
		case 6:
			pos = day05.InstructionJumpIfFalse(memory, pos, mask)
		case 7:
			pos = day05.InstructionLessThan(memory, pos, mask)
		case 8:
			pos = day05.InstructionEqual(memory, pos, mask)
		case 99:
			close(out)
			return
		default:
			panic(fmt.Errorf("unknown intcode %d at position %d", memory[pos], pos))
		}
	}
}

func Amps(phase, program []int) (chan int, chan int) {
	channels := make([]chan int, len(phase)+1)

	for i := range channels {
		channels[i] = make(chan int)
	}

	for i, setting := range phase {
		go Program(utils.CopyIntSlice(program), channels[i], channels[i+1])
		channels[i] <- setting
	}

	return channels[0], channels[len(channels)-1]
}
