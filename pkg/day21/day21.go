package day21

import (
	"fmt"
	"strings"
	"unicode"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
)

const (
	printOutput bool   = false
	WalkMode    string = "WALK"
	RunMode            = "RUN"
)

func WalkHullDamage(input string) int {
	/*
		A B C D  J
		1 1 1 1  0
		? ? ? 0  0
		? ? ? 1  1
		!(A && B && C) && D
	*/
	instructions := []string{
		"OR A J",
		"AND B J",
		"AND C J",
		"NOT J J",
		"AND D J",
	}
	return Springdroid(input, instructions, WalkMode)
}

func RunHullDamage(input string) int {
	/*
		A B C D E H  J
		1 1 1 1 ? ?  0 // part 1
		? ? ? 0 ? ?  0 // part 1
		? ? ? 1 ? ?  1 // part 1
		? ? ? 1 0 0  0
		part 1 && (E || H)
	*/
	instructions := []string{
		"OR A J",
		"AND B J",
		"AND C J",
		"NOT J J",
		"AND D J",

		"OR E T",
		"OR H T",
		"AND T J",
	}
	return Springdroid(input, instructions, RunMode)
}

func Springdroid(input string, springscript []string, mode string) int {
	if len(springscript) > 15 {
		panic("springdroid can only remember at most 15 springscript instructions")
	}

	intCode := day02.ParseIntcode(input)
	in := make(chan int)
	out := make(chan int)
	go day09.Computer(intCode, in, out)

	var buf strings.Builder

	for o := range out {
		if o == '\n' {
			message := buf.String()

			if printOutput {
				fmt.Println(message)
			}

			switch message {
			case "Input instructions:":
				send(strings.Join(springscript, "\n")+"\n"+mode+"\n", in)
			}

			buf.Reset()
		} else if !isASCII(o) {
			if printOutput && buf.Len() > 0 {
				fmt.Println(buf.String())
			}
			return o
		} else {
			buf.WriteByte(byte(o))
		}

	}
	return -1
}

func isASCII(i int) bool {
	return i >= 0 && i <= unicode.MaxASCII
}

func send(input string, in chan<- int) {
	for _, c := range input {
		in <- int(c)
		if printOutput {
			fmt.Printf("%c", c)
		}
	}
}
