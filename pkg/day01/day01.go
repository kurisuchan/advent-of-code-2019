package day01

func ModuleMass(modules []int) int {
	var fuel int
	for _, mass := range modules {
		fuel += FuelRequirement(mass)
	}
	return fuel
}

func AdjustedModuleMass(modules []int) int {
	var fuel int
	for _, mass := range modules {
		req := FuelRequirement(mass)
		for req > 0 {
			fuel += req
			req = FuelRequirement(req)
		}
	}
	return fuel
}

func FuelRequirement(mass int) int {
	return (mass / 3) - 2
}
