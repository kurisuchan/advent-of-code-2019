package day01_test

import (
	"testing"

	"advent-of-code-2019/pkg/day01"
)

func TestModuleMass(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{12, 2},
		{14, 2},
		{1969, 654},
		{100756, 33583},
	}

	for _, test := range tests {
		actual := day01.ModuleMass([]int{test.in})
		if actual != test.out {
			t.Errorf("ModuleMass(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAdjustedModuleMass(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{14, 2},
		{1969, 966},
		{100756, 50346},
	}

	for _, test := range tests {
		actual := day01.AdjustedModuleMass([]int{test.in})
		if actual != test.out {
			t.Errorf("AdjustedModuleMass(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}
