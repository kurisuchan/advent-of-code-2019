package day11

import (
	"strings"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day08"
	"advent-of-code-2019/pkg/day09"
	"advent-of-code-2019/pkg/utils"
)

const (
	colorBlack = 0
	colorWhite = 1
)

func AreaEstimate(input string) int {
	panels := Paint(input, colorBlack)
	return len(panels)
}

func RegistrationIdentifier(input string) string {
	panels := Paint(input, colorWhite)

	var minX, minY, maxX, maxY int
	for coord := range panels {
		minX = utils.MinInt(minX, coord.X)
		minY = utils.MinInt(minY, coord.Y)
		maxX = utils.MaxInt(maxX, coord.X)
		maxY = utils.MaxInt(maxY, coord.Y)
	}

	var out strings.Builder

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			switch panels[utils.Coordinate{X: x, Y: y}] {
			case colorBlack:
				out.WriteString(day08.PixelBlack)
			case colorWhite:
				out.WriteString(day08.PixelWhite)
			}
		}
		out.WriteString("\n")
	}

	return out.String()
}

func Paint(input string, startingColor int) map[utils.Coordinate]int {
	panels := make(map[utils.Coordinate]int)
	position := utils.Coordinate{X: 0, Y: 0}
	direction := utils.NewMovement(
		utils.Coordinate{X: 0, Y: -1},
		utils.Coordinate{X: 1, Y: 0},
		utils.Coordinate{X: 0, Y: 1},
		utils.Coordinate{X: -1, Y: 0},
	)

	intCode := day02.ParseIntcode(input)
	in := make(chan int, 1)
	out := make(chan int, 2)

	go day09.Computer(intCode, in, out)
	in <- startingColor

	for {
		color, ok := <-out
		if !ok {
			break
		}
		turn, ok := <-out
		if !ok {
			break
		}
		panels[position] = color

		if turn == 0 {
			direction = direction.Prev()
		} else {
			direction = direction.Next()
		}

		position.X += direction.Value.(utils.Coordinate).X
		position.Y += direction.Value.(utils.Coordinate).Y

		in <- panels[position]
	}

	return panels
}
