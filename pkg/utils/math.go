package utils

func AbsInt(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func GCD(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a

}

func LCM(a, b int, more ...int) int {
	result := AbsInt(a*b) / GCD(a, b)

	for i := 0; i < len(more); i++ {
		result = LCM(result, more[i])
	}

	return result
}
