package utils

import (
	"container/ring"
	"math"
)

type Coordinate struct {
	X, Y int
}

func (c Coordinate) Distance(t Coordinate) int {
	return AbsInt(c.X-t.X) + AbsInt(c.Y-t.Y)
}

func (c Coordinate) Angle(t Coordinate) float64 {
	angle := math.Atan2(float64(t.X-c.X), float64(c.Y-t.Y)) * 180 / math.Pi
	if angle < 0 {
		angle = angle + 360
	}
	return angle
}

func (c Coordinate) Move(t Coordinate) Coordinate {
	return Coordinate{
		X: c.X + t.X,
		Y: c.Y + t.Y,
	}
}

func NewMovement(directions ...Coordinate) *ring.Ring {
	r := ring.New(len(directions))
	for _, coord := range directions {
		r.Value = coord
		r = r.Next()
	}
	return r
}

var Cardinals = []Coordinate{
	{X: 0, Y: -1},
	{X: 1, Y: 0},
	{X: 0, Y: 1},
	{X: -1, Y: 0},
}

var Diagonals = []Coordinate{
	{X: 1, Y: -1},
	{X: 1, Y: 1},
	{X: -1, Y: 1},
	{X: -1, Y: -1},
}
