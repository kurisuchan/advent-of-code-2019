package utils

import "sort"

type Queue struct {
	items      []interface{}
	priorities map[interface{}]int
}

func NewQueue() *Queue {
	return &Queue{
		priorities: make(map[interface{}]int),
	}
}

func (q *Queue) Check(item interface{}) (int, bool) {
	priority, ok := q.priorities[item]
	return priority, ok
}

func (q *Queue) IsEmpty() bool {
	return len(q.items) == 0
}

func (q *Queue) Len() int {
	return len(q.items)
}

func (q *Queue) Less(i, j int) bool {
	return q.priorities[q.items[i]] < q.priorities[q.items[j]]
}

func (q *Queue) Pop() (item interface{}, priority int) {
	item, q.items = q.items[0], q.items[1:]
	priority = q.priorities[item]
	delete(q.priorities, item)
	return item, priority
}

func (q *Queue) Set(item interface{}, priority int) {
	if _, ok := q.priorities[item]; !ok {
		q.items = append(q.items, item)
	}

	q.priorities[item] = priority
	sort.Sort(q)
}

func (q *Queue) Swap(i, j int) {
	q.items[i], q.items[j] = q.items[j], q.items[i]
}
