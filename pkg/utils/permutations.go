package utils

func heapPermutation(k int, in []int, out chan []int) {
	if k <= 1 {
		out <- append([]int(nil), in...)
	} else {
		for i := 0; i < k; i++ {
			heapPermutation(k-1, in, out)
			if k%2 == 0 {
				in[i], in[k-1] = in[k-1], in[i]
			} else {
				in[0], in[k-1] = in[k-1], in[0]
			}
		}
	}
}

func Permutations(min, max int) chan []int {
	in := make([]int, max-min+1)
	for i := range in {
		in[i] = min + i
	}
	out := make(chan []int)
	go func() { heapPermutation(len(in), in, out); close(out) }()
	return out
}
