package utils_test

import (
	"testing"

	"advent-of-code-2019/pkg/utils"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestPermutations(t *testing.T) {
	tests := []struct {
		min, max int
		out      [][]int
	}{
		{42, 42, [][]int{{42}}},
		{0, 1, [][]int{{0, 1}, {1, 0}}},
		{1, 3, [][]int{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}}},
	}

	for _, test := range tests {
		var actual [][]int
		for permutation := range utils.Permutations(test.min, test.max) {
			actual = append(actual, permutation)
		}

		if diff := cmp.Diff(test.out, actual, cmpopts.SortSlices(oneSliceIsLessThanTheOther)); diff != "" {
			t.Errorf("Permutations(%d, %d) => %v, want %v:\n%s", test.min, test.max, actual, test.out, diff)
		}
	}
}

func oneSliceIsLessThanTheOther(a, b []int) bool {
	for i := 0; i < utils.MaxInt(len(a), len(b)); i++ {
		switch {
		case a[i] > b[i]:
			return false
		case a[i] < b[i]:
			return true
		}
	}
	return len(a) < len(b)
}
