package utils_test

import (
	"testing"

	"advent-of-code-2019/pkg/utils"
)

func TestAngle(t *testing.T) {
	tests := []struct {
		current, target utils.Coordinate
		angle           float64
	}{
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: 1, Y: 0},
			0,
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: 2, Y: 0},
			45,
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: 2, Y: 1},
			90,
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: 1, Y: 2},
			180,
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: 0, Y: 1},
			270,
		},
	}
	for _, test := range tests {
		actual := test.current.Angle(test.target)
		if actual != test.angle {
			t.Errorf("%v.Angle(%v) => %f, want %f", test.current, test.target, actual, test.angle)
		}
	}
}

func TestDistance(t *testing.T) {
	tests := []struct {
		current, target utils.Coordinate
		distance        int
	}{
		{
			utils.Coordinate{X: 5, Y: 5},
			utils.Coordinate{X: 0, Y: 0},
			10,
		},
		{
			utils.Coordinate{X: 5, Y: 5},
			utils.Coordinate{X: 0, Y: 5},
			5,
		},
		{
			utils.Coordinate{X: 0, Y: 0},
			utils.Coordinate{X: -1, Y: -1},
			2,
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: -2, Y: -3},
			7,
		},
	}
	for _, test := range tests {
		actual := test.current.Distance(test.target)
		if actual != test.distance {
			t.Errorf("%v.Distance(%v) => %d, want %d", test.current, test.target, actual, test.distance)
		}
	}
}

func TestMove(t *testing.T) {
	tests := []struct {
		current, offset, result utils.Coordinate
	}{
		{
			utils.Coordinate{X: 5, Y: 5},
			utils.Coordinate{X: -10, Y: 5},
			utils.Coordinate{X: -5, Y: 10},
		},
		{
			utils.Coordinate{X: 0, Y: 0},
			utils.Coordinate{X: 0, Y: 1},
			utils.Coordinate{X: 0, Y: 1},
		},
		{
			utils.Coordinate{X: 0, Y: 0},
			utils.Coordinate{X: -1, Y: -1},
			utils.Coordinate{X: -1, Y: -1},
		},
		{
			utils.Coordinate{X: 1, Y: 1},
			utils.Coordinate{X: -2, Y: 0},
			utils.Coordinate{X: -1, Y: 1},
		},
	}
	for _, test := range tests {
		actual := test.current.Move(test.offset)
		if actual != test.result {
			t.Errorf("%v.Move(%v) => %v, want %v", test.current, test.offset, actual, test.result)
		}
	}
}
