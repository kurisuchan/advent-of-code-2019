package utils_test

import (
	"testing"

	"advent-of-code-2019/pkg/utils"
)

func TestGCD(t *testing.T) {
	tests := []struct {
		a, b int
		out  int
	}{
		{54, 24, 6},
		{48, 180, 12},
		{13, 13, 13},
		{37, 600, 1},
		{20, 100, 20},
		{624129, 2061517, 18913},
	}

	for _, test := range tests {
		actual := utils.GCD(test.a, test.b)
		if actual != test.out {
			t.Errorf("GCD(%d, %d) => %d, want %d", test.a, test.b, actual, test.out)
		}
	}
}

func TestLCM(t *testing.T) {
	tests := []struct {
		a, b int
		more []int
		out  int
	}{
		{120, 140, nil, 840},
		{10213, 312, nil, 3186456},
		{10, 30, nil, 30},
		{10, 20, []int{30, 40}, 120},
		{12, 15, []int{10, 75}, 300},
	}

	for _, test := range tests {
		actual := utils.LCM(test.a, test.b, test.more...)
		if actual != test.out {
			t.Errorf("LCM(%d, %d, %v) => %d, want %d", test.a, test.b, test.more, actual, test.out)
		}
	}
}
