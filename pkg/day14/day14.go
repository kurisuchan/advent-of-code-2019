package day14

import (
	"fmt"
	"regexp"
	"strings"

	"advent-of-code-2019/pkg/utils"
)

type Chemicals map[string]Formula

type Formula struct {
	Output int
	Input  map[string]int
}

const (
	fuel     = "FUEL"
	ore      = "ORE"
	trillion = 1000000000000
)

func OreForFuel(input []string) int {
	chemicals := ParseInput(input)

	ore := chemicals.OreDemands(fuel, 1)

	return ore
}

func FuelForOre(input []string) int {
	chemicals := ParseInput(input)

	low := trillion / chemicals.OreDemands(fuel, 1)
	high := low * 2

	for low+1 < high {
		mid := (low + high) / 2

		if chemicals.OreDemands(fuel, mid) > trillion {
			high = mid
		} else {
			low = mid
		}
	}

	return low
}

func ParseInput(input []string) Chemicals {
	formula := regexp.MustCompile(`(\d+) (\w+)`)

	chemicals := make(Chemicals)

	for _, line := range input {
		parts := strings.Split(line, "=>")
		if len(parts) != 2 {
			continue
		}

		input := formula.FindAllStringSubmatch(parts[0], -1)
		output := formula.FindAllStringSubmatch(parts[1], -1)

		f := Formula{
			Output: utils.MustInt(output[0][1]),
			Input:  make(map[string]int, len(input)),
		}

		for _, in := range input {
			f.Input[in[2]] = utils.MustInt(in[1])
		}

		chemicals[output[0][2]] = f
	}

	return chemicals
}

func (c Chemicals) OreDemands(target string, amount int) int {
	if _, ok := c[target]; !ok {
		panic(fmt.Errorf("no formula for %q", target))
	}

	want := make(map[string]int, len(c))
	want[target] = amount

	var demandsMet bool

	for !demandsMet {
		for what := range want {
			if what == ore || want[what] <= 0 {
				continue
			}

			produce := want[what] / c[what].Output
			spill := want[what] % c[what].Output

			want[what] = 0

			if spill != 0 {
				want[what] -= c[what].Output - spill
				produce += 1
			}

			for chemical, amount := range c[what].Input {
				want[chemical] += produce * amount
			}
		}

		for chemical, amount := range want {
			demandsMet = true

			if chemical == ore {
				continue

			}
			if amount > 0 {
				demandsMet = false
				break
			}
		}
	}

	return want[ore]
}
