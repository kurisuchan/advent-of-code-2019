package day12

import (
	"regexp"
	"strconv"

	"advent-of-code-2019/pkg/utils"
)

type Vector struct {
	X, Y, Z int
}

func (v Vector) Sum() int {
	return utils.AbsInt(v.X) + utils.AbsInt(v.Y) + utils.AbsInt(v.Z)
}

type Moon struct {
	Pos Vector
	Vel Vector
}

func (m *Moon) Gravity(o Moon) {
	if m.Pos.X < o.Pos.X {
		m.Vel.X++
	}
	if m.Pos.X > o.Pos.X {
		m.Vel.X--
	}

	if m.Pos.Y < o.Pos.Y {
		m.Vel.Y++
	}
	if m.Pos.Y > o.Pos.Y {
		m.Vel.Y--
	}

	if m.Pos.Z < o.Pos.Z {
		m.Vel.Z++
	}
	if m.Pos.Z > o.Pos.Z {
		m.Vel.Z--
	}
}

func (m *Moon) Step() {
	m.Pos.X += m.Vel.X
	m.Pos.Y += m.Vel.Y
	m.Pos.Z += m.Vel.Z
}

func (m Moon) PotentialEnergy() int {
	return m.Pos.Sum() * m.Vel.Sum()
}

func Part1(input string, steps int) int {
	vectors := ParseInput(input)
	var moons []Moon

	for _, v := range vectors {
		moons = append(moons, Moon{Pos: v})
	}

	for i := 0; i < steps; i++ {
		for a := range moons {
			for b := range moons {
				if a == b {
					continue
				}

				moons[a].Gravity(moons[b])
			}
		}

		for moon := range moons {
			moons[moon].Step()
		}
	}

	var sum int
	for moon := range moons {
		sum += moons[moon].PotentialEnergy()
	}

	return sum
}

func Part2(input string) int {
	vectors := ParseInput(input)
	var moons []Moon

	for _, v := range vectors {
		moons = append(moons, Moon{Pos: v})
	}

	initialState := make([]Moon, len(moons))
	copy(initialState, moons)

	var totalSteps Vector

	for i := 1; ; i++ {
		for a := range moons {
			for b := range moons {
				if a == b {
					continue
				}

				moons[a].Gravity(moons[b])
			}
		}

		// hint: axes move independently
		initialX := totalSteps.X == 0
		initialY := totalSteps.Y == 0
		initialZ := totalSteps.Z == 0

		for moon := range moons {
			moons[moon].Step()

			if initialX && (moons[moon].Pos.X != initialState[moon].Pos.X || moons[moon].Vel.X != initialState[moon].Vel.X) {
				initialX = false
			}

			if initialY && (moons[moon].Pos.Y != initialState[moon].Pos.Y || moons[moon].Vel.Y != initialState[moon].Vel.Y) {
				initialY = false
			}

			if initialZ && (moons[moon].Pos.Z != initialState[moon].Pos.Z || moons[moon].Vel.Z != initialState[moon].Vel.Z) {
				initialZ = false
			}
		}

		if initialX {
			totalSteps.X = i
		}

		if initialY {
			totalSteps.Y = i
		}

		if initialZ {
			totalSteps.Z = i
		}

		if totalSteps.X != 0 && totalSteps.Y != 0 && totalSteps.Z != 0 {
			break
		}
	}

	return utils.LCM(totalSteps.X, totalSteps.Y, totalSteps.Z)
}

func ParseInput(input string) []Vector {
	pattern := regexp.MustCompile(`<x=(-?\d+), y=(-?\d+), z=(-?\d+)>`)
	var vectors []Vector

	for _, match := range pattern.FindAllStringSubmatch(input, -1) {
		var v Vector
		v.X, _ = strconv.Atoi(match[1])
		v.Y, _ = strconv.Atoi(match[2])
		v.Z, _ = strconv.Atoi(match[3])

		vectors = append(vectors, v)
	}

	return vectors
}
