package day12_test

import (
	"testing"

	"advent-of-code-2019/pkg/day12"
)

func TestPart1(t *testing.T) {
	tests := []struct {
		in  string
		steps int
		out int
	}{
		{`<x=-1, y=0, z=2><x=2, y=-10, z=-7><x=4, y=-8, z=8><x=3, y=5, z=-1>`, 10, 179},
		{`<x=-8, y=-10, z=0><x=5, y=5, z=10><x=2, y=-7, z=3><x=9, y=-8, z=-3>`, 100, 1940},
	}

	for _, test := range tests {
		actual := day12.Part1(test.in, test.steps)
		if actual != test.out {
			t.Errorf("Part1(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestPart2(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`<x=-1, y=0, z=2><x=2, y=-10, z=-7><x=4, y=-8, z=8><x=3, y=5, z=-1>`, 2772},
		{`<x=-8, y=-10, z=0><x=5, y=5, z=10><x=2, y=-7, z=3><x=9, y=-8, z=-3>`, 4686774924},
	}

	for _, test := range tests {
		actual := day12.Part2(test.in)
		if actual != test.out {
			t.Errorf("Part2(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
