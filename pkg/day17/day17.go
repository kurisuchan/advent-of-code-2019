package day17

import (
	"fmt"
	"strconv"
	"strings"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
	"advent-of-code-2019/pkg/utils"
)

type View map[utils.Coordinate]int

const debug = false

const (
	CharScaffold    = '#'
	CharSpace       = '.'
	CharNewLine     = '\n'
	CharRobotUp     = '^'
	CharRobotRight  = '>'
	CharRobotDown   = 'v'
	CharRobotLeft   = '<'
	RobotDirections = "^>v<"
	TurnLeft        = "L"
	TurnRight       = "R"
)

var directions = utils.NewMovement(
	utils.Coordinate{X: 0, Y: -1},
	utils.Coordinate{X: 1, Y: 0},
	utils.Coordinate{X: 0, Y: 1},
	utils.Coordinate{X: -1, Y: 0},
)

func Part1(input string) int {
	grid, _, _ := CameraView(input)

	var checksum int
	for c := range grid {
		intersection := true
		directions.Do(func(p interface{}) {
			n := c.Move(p.(utils.Coordinate))
			if _, ok := grid[n]; !ok {
				intersection = false
			}
		})
		if intersection {
			checksum += c.X * c.Y
		}
	}

	return checksum
}

func Part2(input string) int {
	grid, robot, direction := CameraView(input)

	currentDirection := directions.Move(direction)

	var instructions []string

	for {
		var steps, turn int
		end := true
		for _, direction := range [2]int{-1, 1} {
			if _, ok := grid[robot.Move(currentDirection.Move(direction).Value.(utils.Coordinate))]; ok {
				currentDirection = currentDirection.Move(direction)
				turn = direction
				end = false
			}
		}
		if end {
			break
		}

		if turn == -1 {
			instructions = append(instructions, TurnLeft)
		} else {
			instructions = append(instructions, TurnRight)
		}

		for {
			next := robot.Move(currentDirection.Value.(utils.Coordinate))
			if _, ok := grid[next]; ok {
				steps++
				robot = next
			} else {
				break
			}
		}

		instructions = append(instructions, strconv.Itoa(steps))
	}

	// FIXME: Compress and convert `instructions` to `sequences` here :-)
	sequences := map[string]string {
		"main": "C,A,C,A,C,B,A,B,C,B",
		"A": "R,12,L,10,L,4,L,6",
		"B":"L,10,L,10,L,4,L,6",
		"C":"L,6,R,12,L,6",
	}

	intCode := day02.ParseIntcode(input)
	intCode[0] = 2
	in := make(chan int, 1)
	out := make(chan int, 1)
	go day09.Computer(intCode, in, out)

	var buf strings.Builder

	for o := range out {
		// After inputs program will print finished map and then the score
		if o > 0xFF {
			return o
		}

		buf.WriteString(string(o))

		if o == CharNewLine {
			output := buf.String()

			printDebug(output)

			switch output {
			case "Main:\n":
				writeSequence(in, sequences["main"])
			case "Function A:\n":
				writeSequence(in, sequences["A"])
			case "Function B:\n":
				writeSequence(in, sequences["B"])
			case "Function C:\n":
				writeSequence(in, sequences["C"])
			case "Continuous video feed?\n":
				writeSequence(in, "n")
			}
			buf.Reset()
		}
	}
	return 0
}

func printDebug(out interface{}) {
	if debug {
		switch o := out.(type) {
		case int:
			fmt.Print(string(o))
		case rune:
			fmt.Print(string(o))
		case string:
			fmt.Print(o)
		}
	}
}

func writeSequence(out chan<- int, sequence string) {
	for _, r := range sequence {
		out <- int(r)
		printDebug(r)
	}
	out <- CharNewLine
	printDebug(CharNewLine)
}

func CameraView(input string) (View, utils.Coordinate, int) {
	grid := make(View)
	intCode := day02.ParseIntcode(input)
	in := make(chan int)
	out := make(chan int)
	go day09.Computer(intCode, in, out)

	var x, y, direction int
	var robot utils.Coordinate
	for r := range out {
		pos := utils.Coordinate{X: x, Y: y}
		switch r {
		case CharNewLine:
			x = 0
			y += 1
		case CharRobotUp, CharRobotRight, CharRobotDown, CharRobotLeft:
			direction = strings.IndexRune(RobotDirections, rune(r))
			robot = pos
			grid[pos] = r
			x++
		case CharSpace: // ignore
			x++
		case CharScaffold:
			grid[pos] = r
			x++
		}
	}
	return grid, robot, direction
}
