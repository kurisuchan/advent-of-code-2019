package day04_test

import (
	"reflect"
	"testing"

	"advent-of-code-2019/pkg/day04"
)

func TestCountPasswords(t *testing.T) {
	tests := []struct {
		in        string
		validator day04.PasswordValidator
		out       int
	}{
		{"000000-000000", day04.ValidPassword, 1},
		{"999999-999999", day04.ValidPassword, 1},
		{"000000-000011", day04.ValidPassword, 11},
		{"000000-000000", day04.AdditionallyValidPassword, 0},
		{"000000-000011", day04.AdditionallyValidPassword, 1},
	}

	for _, test := range tests {
		actual := day04.CountPasswords(test.in, test.validator)
		if actual != test.out {
			t.Errorf("ValidPassword(%s) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestValidPassword(t *testing.T) {
	tests := []struct {
		in  [6]int
		out bool
	}{
		{[6]int{1, 1, 1, 1, 1, 1}, true},
		{[6]int{2, 2, 3, 4, 5, 0}, false},
		{[6]int{1, 2, 3, 7, 8, 9}, false},
	}

	for _, test := range tests {
		actual := day04.ValidPassword(test.in)
		if actual != test.out {
			t.Errorf("ValidPassword(%v) => %t, want %t", test.in, actual, test.out)
		}
	}
}

func TestAdditionallyValidPassword(t *testing.T) {
	tests := []struct {
		in  [6]int
		out bool
	}{
		{[6]int{1, 1, 1, 1, 1, 1}, false},
		{[6]int{2, 2, 3, 4, 5, 0}, false},
		{[6]int{1, 2, 3, 7, 8, 9}, false},
		{[6]int{1, 1, 2, 2, 3, 3}, true},
		{[6]int{1, 2, 3, 4, 4, 4}, false},
		{[6]int{1, 1, 1, 1, 2, 2}, true},
		{[6]int{1, 1, 2, 2, 2, 2}, true},
	}

	for _, test := range tests {
		actual := day04.AdditionallyValidPassword(test.in)
		if actual != test.out {
			t.Errorf("AdditionallyValidPassword(%v) => %t, want %t", test.in, actual, test.out)
		}
	}
}

func TestIntToSlice(t *testing.T) {
	tests := []struct {
		in  int
		out [6]int
	}{
		{123456, [6]int{1, 2, 3, 4, 5, 6}},
	}

	for _, test := range tests {
		actual := day04.IntToSlice(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("Y(%d) => %v, want %v", test.in, actual, test.out)
		}
	}
}
