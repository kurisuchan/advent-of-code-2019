package day04

import (
	"fmt"
	"strconv"
	"strings"
)

func CountPasswords(input string, validator PasswordValidator) int {
	min, max := ParseInput(input)

	var count int

	for i := min; i <= max; i++ {
		if validator(IntToSlice(i)) {
			count++
		}
	}

	return count
}

type PasswordValidator func([6]int) bool

func ValidPassword(password [6]int) bool {
	doubles := false
	min := password[0]

	for i := 1; i < 6; i++ {
		if password[i] < password[i-1] || password[i] < min {
			return false
		}

		if password[i] == password[i-1] {
			doubles = true
		}

		min = password[i]
	}

	return doubles
}

func AdditionallyValidPassword(password [6]int) bool {
	var digits [10]int
	min := password[0]
	digits[password[0]]++

	for i := 1; i < 6; i++ {
		digits[password[i]]++

		if password[i] < password[i-1] || password[i] < min {
			return false
		}

		min = password[i]
	}

	for _, count := range digits {
		if count == 2 {
			return true
		}
	}

	return false
}

func ParseInput(input string) (int, int) {
	input = strings.TrimSpace(input)
	inputs := strings.Split(input, "-")

	if len(inputs) != 2 {
		panic(fmt.Errorf("not two inputs: %v", inputs))
	}

	min, err := strconv.Atoi(inputs[0])
	if err != nil {
		panic(err.Error())
	}
	max, err := strconv.Atoi(inputs[1])
	if err != nil {
		panic(err.Error())
	}
	return min, max
}

func IntToSlice(x int) [6]int {
	var out [6]int
	for i := 5; i >= 0; i-- {
		out[i] = x % 10
		x /= 10
	}
	return out
}
