package day03

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"advent-of-code-2019/pkg/utils"
)

type Instruction struct {
	Direction string
	Distance  int
}

func ParseInput(input string) [][]Instruction {
	var instructions [][]Instruction
	for _, row := range strings.Split(input, "\n") {
		var wire []Instruction
		for _, ins := range strings.Split(row, ",") {
			if len(ins) < 2 {
				continue
			}

			dist, err := strconv.Atoi(ins[1:])
			if err != nil {
				panic(err.Error())
			}

			wire = append(wire, Instruction{
				Direction: string(ins[0]),
				Distance:  dist,
			})
		}
		instructions = append(instructions, wire)
	}
	return instructions
}

func Part1(input string) int {
	instructions := ParseInput(input)
	grid := make(map[int]map[int]int)
	shortest := math.MaxInt64

	for i, wire := range instructions {
		var posX, posY int

		for _, step := range wire {
			for step.Distance > 0 {

				switch step.Direction {
				case "U":
					posY += 1
				case "D":
					posY -= 1
				case "L":
					posX -= 1
				case "R":
					posX += 1
				default:
					panic(fmt.Errorf("cannot handle direction %s", step.Direction))
				}

				if grid[posX] == nil {
					grid[posX] = make(map[int]int)
				}

				if i == 0 {
					grid[posX][posY] = 1
				} else {
					if grid[posX][posY] > 0 {
						distance := utils.AbsInt(posX) + utils.AbsInt(posY)
						if distance < shortest {
							shortest = distance
						}
					}
				}
				step.Distance -= 1
			}
		}
	}
	return shortest
}
func Part2(input string) int {
	instructions := ParseInput(input)
	grid := make(map[int]map[int]int)
	shortest := math.MaxInt64

	for i, wire := range instructions {
		var posX, posY, length int

		for _, step := range wire {
			for step.Distance > 0 {
				length += 1

				switch step.Direction {
				case "U":
					posY += 1
				case "D":
					posY -= 1
				case "L":
					posX -= 1
				case "R":
					posX += 1
				default:
					panic(fmt.Errorf("cannot handle direction %s", step.Direction))
				}

				if grid[posX] == nil {
					grid[posX] = make(map[int]int)
				}

				if i == 0 {
					if _, ok := grid[posX][posY]; !ok {
						grid[posX][posY] = length
					}
				} else {
					if grid[posX][posY] > 0 {
						distance := grid[posX][posY] + length
						if distance < shortest {
							shortest = distance
						}
					}
				}
				step.Distance -= 1
			}
		}
	}
	return shortest
}
