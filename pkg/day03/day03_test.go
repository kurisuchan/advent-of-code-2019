package day03_test

import (
	"testing"

	"advent-of-code-2019/pkg/day03"
)

func TestPart1(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"R8,U5,L5,D3\nU7,R6,D4,L4\n", 6},
		{"R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 159},
		{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135},
	}

	for _, test := range tests {
		actual := day03.Part1(test.in)
		if actual != test.out {
			t.Errorf("Part1(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestPart2(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"R8,U5,L5,D3\nU7,R6,D4,L4\n", 30},
		{"R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 610},
		{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410},
	}

	for _, test := range tests {
		actual := day03.Part2(test.in)
		if actual != test.out {
			t.Errorf("Part2(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
