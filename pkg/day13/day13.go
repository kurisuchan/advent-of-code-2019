package day13

import (
	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
	"advent-of-code-2019/pkg/utils"
)

const (
	EmptyTile = iota
	WallTile
	BlockTile
	PaddleTile
	BallTile
)

func CountBlocks(input string) int {
	grid := make(map[utils.Coordinate]int)

	intCode := day02.ParseIntcode(input)
	in := make(chan int)
	out := make(chan int, 3)
	go day09.Computer(intCode, in, out)

	for {
		x, ok := <-out
		if !ok {
			break
		}
		y := <-out
		tile := <-out

		grid[utils.Coordinate{X: x, Y: y}] = tile
	}

	var count int

	for _, v := range grid {
		if v == BlockTile {
			count++
		}
	}

	return count
}

func BeatTheGame(input string) int {
	grid := make(map[utils.Coordinate]int)

	intCode := day02.ParseIntcode(input)
	intCode[0] = 2
	in := make(chan int, 1)
	out := make(chan int, 3)
	go day09.Computer(intCode, in, out)

	var paddle, ball utils.Coordinate
	score := utils.Coordinate{X: -1, Y: 0}

	for {
		x, ok := <-out
		if !ok {
			break
		}
		y := <-out
		tile := <-out
		grid[utils.Coordinate{X: x, Y: y}] = tile

		switch tile {
		case BallTile:
			ball = utils.Coordinate{X: x, Y: y}

			switch {
			case paddle.X > ball.X:
				in <- -1

			case paddle.X < ball.X:
				in <- 1

			default:
				in <- 0
			}

		case PaddleTile:
			paddle = utils.Coordinate{X: x, Y: y}
		}
	}

	return grid[score]
}
