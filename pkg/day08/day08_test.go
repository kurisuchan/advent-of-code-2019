package day08_test

import (
	"strings"
	"testing"

	"advent-of-code-2019/pkg/day08"
)

func TestImageChecksum(t *testing.T) {
	tests := []struct {
		in            string
		width, height int
		out           int
	}{
		{"123456789012", 2, 2, 1},
	}

	for _, test := range tests {
		actual := day08.ImageChecksum(test.in, test.width, test.height)
		if actual != test.out {
			t.Errorf("ImageChecksum(%q, %d, %d) => %d, want %d", test.in, test.width, test.height, actual, test.out)
		}
	}
}

func TestImageRender(t *testing.T) {
	tests := []struct {
		in            string
		width, height int
		out           string
	}{
		{"0222112222120000", 2, 2, strings.Join([]string{
			day08.PixelBlack + day08.PixelWhite,
			day08.PixelWhite + day08.PixelBlack,
		}, "\n") + "\n"},
	}

	for _, test := range tests {
		actual := day08.ImageRender(test.in, test.width, test.height)
		if actual != test.out {
			t.Errorf("ImageRender(%q, %d, %d) =>\n%swant\n%s", test.in, test.width, test.height, actual, test.out)
		}
	}
}
