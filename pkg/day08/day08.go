package day08

import (
	"math"
	"regexp"
	"strings"
)

const (
	Black       = '0'
	White       = '1'
	Transparent = '2'
)

const (
	PixelBlack       = "█"
	PixelWhite       = "░"
	PixelTransparent = " "
	PixelWhoops      = "▞"
)

func ImageChecksum(input string, width, height int) int {
	var count0, count1, count2, minResult int
	min0 := math.MaxInt64

	for i, r := range input {
		switch r {
		case '0':
			count0++
		case '1':
			count1++
		case '2':
			count2++
		}

		if (i+1)%(width*height) == 0 {
			if count0 < min0 {
				min0 = count0
				minResult = count1 * count2
			}
			count0, count1, count2 = 0, 0, 0
		}
	}

	return minResult
}

func ImageRender(input string, width, height int) string {
	var layers [][]rune
	layer := make([]rune, width*height)

	reg := regexp.MustCompile(`[^012]`)
	input = reg.ReplaceAllString(input, "")

	for i, r := range input {
		layer[i%(width*height)] = r

		if (i+1)%(width*height) == 0 {
			layers = append(layers, layer)
			layer = make([]rune, width*height)
		}
	}

	var out strings.Builder
	out.Grow((width * len(PixelBlack)) * height + height)

pixels:
	for i := 0; i < width*height; i++ {
		for l := range layers {
			switch layers[l][i] {
			case Transparent:
				continue
			case Black:
				out.WriteString(PixelBlack)
			case White:
				out.WriteString(PixelWhite)
			default:
				out.WriteString(PixelWhoops)
			}
			if (i+1)%(width) == 0 {
				out.WriteString("\n")
			}
			continue pixels
		}

		out.WriteString(PixelTransparent)
	}

	return out.String()
}
