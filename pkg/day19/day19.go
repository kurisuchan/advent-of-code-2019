package day19

import (
	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
)

const (
	sleigh   int = 100
	offByOne int = -1
)

func Part1(input string) int {
	intCode := day02.ParseIntcode(input)

	var affected int

	for y := 0; y < 50; y++ {
		for x := 0; x < 50; x++ {
			if Check(intCode, x, y) {
				affected++
			}
		}
	}
	return affected
}

func Part2(input string) int {
	intCode := day02.ParseIntcode(input)

	posX, posY := 0, sleigh

	for {
		for !Check(intCode, posX, posY) {
			posX++
		}

		if Check(intCode, posX, posY) && // bottom left
			Check(intCode, posX+(sleigh+offByOne), posY) && // bottom right
			Check(intCode, posX, posY-(sleigh+offByOne)) && // top left
			Check(intCode, posX+(sleigh+offByOne), posY-(sleigh+offByOne)) { // top right

			return posX*10000 + (posY - (sleigh + offByOne))
		}

		posY++
	}
}

func Check(intCode []int, x, y int) bool {
	in := make(chan int, 2)
	out := make(chan int)
	go day09.Computer(intCode, in, out)
	in <- x
	in <- y
	return <-out == 1
}
