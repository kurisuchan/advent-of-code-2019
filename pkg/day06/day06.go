package day06

import (
	"strings"
)

const (
	santa                 = "SAN"
	you                   = "YOU"
	universalCenterOfMass = "COM"
)

type OrbitMap map[string]*Object

type Object struct {
	Name     string
	Children []*Object
	Depth    int
}

func (o *Object) SetDepth(d int) {
	o.Depth = d
	for _, child := range o.Children {
		child.SetDepth(d + 1)
	}
}

func (o *Object) SumDepths() int {
	sum := o.Depth
	for _, child := range o.Children {
		sum += child.SumDepths()
	}
	return sum
}

func (o *Object) Has(name string) bool {
	if o.Name == name {
		return true
	}
	for _, child := range o.Children {
		if child.Has(name) {
			return true
		}
	}
	return false
}

func FindCommonAncestor(om OrbitMap, current *Object) *Object {
	for _, child := range current.Children {
		if child.Has(you) && child.Has(santa) {
			return FindCommonAncestor(om, child)
		}
	}
	return current
}

func ParseInput(input []string) OrbitMap {
	om := make(OrbitMap)

	for _, line := range input {
		obj := strings.Split(strings.TrimSpace(line), ")")
		if len(obj) != 2 {
			continue
		}
		om[obj[0]] = &Object{Name: obj[0]}
		om[obj[1]] = &Object{Name: obj[1]}
	}

	for _, line := range input {
		obj := strings.Split(strings.TrimSpace(line), ")")
		if len(obj) != 2 {
			continue
		}
		om[obj[0]].Children = append(om[obj[0]].Children, om[obj[1]])
	}

	om[universalCenterOfMass].SetDepth(0)
	return om
}

func OrbitCountChecksum(om OrbitMap) int {
	return om[universalCenterOfMass].SumDepths()
}

func OrbitalTransfers(om OrbitMap) int {
	commonAncestor := FindCommonAncestor(om, om[universalCenterOfMass])
	return om[you].Depth - 1 + om[santa].Depth - 1 - 2*commonAncestor.Depth
}
