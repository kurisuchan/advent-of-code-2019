package day06_test

import (
	"testing"

	"advent-of-code-2019/pkg/day06"
)

func TestOrbitCountChecksum(t *testing.T) {
	tests := []struct {
		in  day06.OrbitMap
		out int
	}{
		{day06.ParseInput([]string{
			"COM)B",
			"B)C",
			"C)D",
			"D)E",
			"E)F",
			"B)G",
			"G)H",
			"D)I",
			"E)J",
			"J)K",
			"K)L",
		}), 42},
	}

	for i, test := range tests {
		actual := day06.OrbitCountChecksum(test.in)
		if actual != test.out {
			t.Errorf("OrbitCountChecksum(%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestOrbitalTransfers(t *testing.T) {
	tests := []struct {
		in  day06.OrbitMap
		out int
	}{
		{day06.ParseInput([]string{
			"COM)B",
			"B)C",
			"C)D",
			"D)E",
			"E)F",
			"B)G",
			"G)H",
			"D)I",
			"E)J",
			"J)K",
			"K)L",
			"K)YOU",
			"I)SAN",
		}), 4},
	}

	for i, test := range tests {
		actual := day06.OrbitalTransfers(test.in)
		if actual != test.out {
			t.Errorf("OrbitalTransfers(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
