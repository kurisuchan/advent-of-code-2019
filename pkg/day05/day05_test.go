package day05_test

import (
	"reflect"
	"runtime"
	"testing"

	"advent-of-code-2019/pkg/day05"
)

func TestProgram(t *testing.T) {
	tests := []struct {
		memory []int
		input  int
		out    []int
	}{
		{[]int{1002, 4, 3, 4, 33}, 0, nil},
		{[]int{1101, 100, -1, 4, 0}, 0, nil},
		{[]int{3, 0, 4, 0, 99}, 0, []int{0}},
		{[]int{3, 0, 4, 0, 99}, 123, []int{123}},
		{[]int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}, 7, []int{0}},
		{[]int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}, 8, []int{1}},
		{[]int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8}, 7, []int{1}},
		{[]int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8}, 8, []int{0}},
		{[]int{3, 3, 1108, -1, 8, 3, 4, 3, 99}, 7, []int{0}},
		{[]int{3, 3, 1108, -1, 8, 3, 4, 3, 99}, 8, []int{1}},
		{[]int{3, 3, 1107, -1, 8, 3, 4, 3, 99}, 7, []int{1}},
		{[]int{3, 3, 1107, -1, 8, 3, 4, 3, 99}, 8, []int{0}},
		{[]int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9}, 0, []int{0}},
		{[]int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9}, 1, []int{1}},
		{[]int{3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1}, 0, []int{0}},
		{[]int{3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1}, 1, []int{1}},
		{
			[]int{
				3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
				1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
				999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99,
			},
			7, []int{999},
		},
		{
			[]int{
				3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
				1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
				999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99,
			},
			8, []int{1000},
		},
		{
			[]int{
				3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
				1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
				999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99,
			},
			9, []int{1001},
		},
	}

	for _, test := range tests {
		actual := day05.Program(test.memory, test.input)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("Program(%v, %d) => %v, want %v", test.memory, test.input, actual, test.out)
		}
	}
}

func TestDiagnosticsPrograms(t *testing.T) {
	tests := []struct {
		program func(string) int
		input   string
		out     int
	}{
		{day05.AirConditioner, "3,0,104,0,4,0,99", 1},
		{day05.ThermalRadiators, "3,0,4,0,99", 5},
	}

	for _, test := range tests {
		actual := test.program(test.input)
		if actual != test.out {
			t.Errorf("%s(%q) => %d, want %d", runtime.FuncForPC(reflect.ValueOf(test.program).Pointer()).Name(), test.input, actual, test.out)
		}
	}
}
