package day05

import (
	"fmt"

	"advent-of-code-2019/pkg/day02"
)

func AirConditioner(input string) int {
	intCode := day02.ParseIntcode(input)
	out := Program(intCode, 1)

	if out == nil || len(out) < 2 {
		panic(fmt.Errorf("diagnostics failed: %v", out))
	}
	for i := 0; i < len(out)-1; i++ {
		if out[i] != 0 {
			panic(fmt.Errorf("diagnostics failed: %v", out))
		}
	}

	return out[len(out)-1]
}

func ThermalRadiators(input string) int {
	intCode := day02.ParseIntcode(input)
	out := Program(intCode, 5)
	return out[0]
}

func Program(memory []int, input int) []int {
	var out []int
	var pos, mask, output int
	for {
		mask = memory[pos] / 100
		switch memory[pos] % 100 {
		case 1:
			pos = InstructionAdd(memory, pos, mask)
		case 2:
			pos = InstructionMultiply(memory, pos, mask)
		case 3:
			pos = InstructionInput(memory, pos, mask, input)
		case 4:
			pos, output = InstructionOutput(memory, pos, mask)
			out = append(out, output)
		case 5:
			pos = InstructionJumpIfTrue(memory, pos, mask)
		case 6:
			pos = InstructionJumpIfFalse(memory, pos, mask)
		case 7:
			pos = InstructionLessThan(memory, pos, mask)
		case 8:
			pos = InstructionEqual(memory, pos, mask)
		case 99:
			return out
		default:
			panic(fmt.Errorf("unknown intcode %d at position %d", memory[pos], pos))
		}
	}
}

func Read(memory []int, pos, mask, arguments int) []int {
	var out []int
	for i := 1; i <= arguments; i++ {
		switch mask % 10 {
		case 0:
			out = append(out, memory[memory[pos+i]])
		case 1:
			out = append(out, memory[pos+i])
		default:
			panic(fmt.Errorf("unknown mode: %d", i%10))
		}
		mask = mask / 10
	}
	return out
}

func InstructionAdd(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	memory[memory[pos+3]] = data[0] + data[1]
	return pos + 4
}

func InstructionMultiply(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	memory[memory[pos+3]] = data[0] * data[1]
	return pos + 4
}

func InstructionInput(memory []int, pos, mask, value int) int {
	memory[memory[pos+1]] = value
	return pos + 2
}

func InstructionOutput(memory []int, pos, mask int) (int, int) {
	data := Read(memory, pos, mask, 1)
	return pos + 2, data[0]
}

func InstructionJumpIfTrue(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	if data[0] != 0 {
		return data[1]
	}
	return pos + 3
}

func InstructionJumpIfFalse(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	if data[0] == 0 {
		return data[1]
	}
	return pos + 3
}

func InstructionLessThan(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	if data[0] < data[1] {
		memory[memory[pos+3]] = 1
	} else {
		memory[memory[pos+3]] = 0
	}
	return pos + 4
}

func InstructionEqual(memory []int, pos, mask int) int {
	data := Read(memory, pos, mask, 2)
	if data[0] == data[1] {
		memory[memory[pos+3]] = 1
	} else {
		memory[memory[pos+3]] = 0
	}
	return pos + 4
}
