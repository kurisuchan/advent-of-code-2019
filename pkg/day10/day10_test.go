package day10_test

import (
	"testing"

	"advent-of-code-2019/pkg/day10"
	"advent-of-code-2019/pkg/utils"
)

func TestBestLocation(t *testing.T) {
	tests := []struct {
		in    []string
		best  utils.Coordinate
		count int
	}{
		{
			[]string{
				".#..#",
				".....",
				"#####",
				"....#",
				"...##",
			},
			utils.Coordinate{X: 3, Y: 4},
			8,
		},
		{
			[]string{
				"......#.#.",
				"#..#.#....",
				"..#######.",
				".#.#.###..",
				".#..#.....",
				"..#....#.#",
				"#..#....#.",
				".##.#..###",
				"##...#..#.",
				".#....####",
			},
			utils.Coordinate{X: 5, Y: 8},
			33,
		},
		{
			[]string{
				"#.#...#.#.",
				".###....#.",
				".#....#...",
				"##.#.#.#.#",
				"....#.#.#.",
				".##..###.#",
				"..#...##..",
				"..##....##",
				"......#...",
				".####.###.",
			},
			utils.Coordinate{X: 1, Y: 2},
			35,
		},
		{
			[]string{
				".#..#..###",
				"####.###.#",
				"....###.#.",
				"..###.##.#",
				"##.##.#.#.",
				"....###..#",
				"..#.#..#.#",
				"#..#.#.###",
				".##...##.#",
				".....#.#..",
			},
			utils.Coordinate{X: 6, Y: 3},
			41,
		},
		{
			[]string{
				".#..##.###...#######",
				"##.############..##.",
				".#.######.########.#",
				".###.#######.####.#.",
				"#####.##.#.##.###.##",
				"..#####..#.#########",
				"####################",
				"#.####....###.#.#.##",
				"##.#################",
				"#####.##.###..####..",
				"..######..##.#######",
				"####.##.####...##..#",
				".#####..#.######.###",
				"##...#.##########...",
				"#.##########.#######",
				".####.#.###.###.#.##",
				"....##.##.###..#####",
				".#.#.###########.###",
				"#.#.#.#####.####.###",
				"###.##.####.##.#..##",
			},
			utils.Coordinate{X: 11, Y: 13},
			210,
		},
	}

	for i, test := range tests {
		actualCoordinate, actualCount := day10.BestLocation(test.in)
		if actualCoordinate != test.best || actualCount != test.count {
			t.Errorf("BestLocation(%d) => %v,%d, want %v,%d", i, actualCoordinate, actualCount, test.best, test.count)
		}
	}
}

func TestCompleteVaporizationBet(t *testing.T) {
	type bet struct {
		bet int
		out int
	}

	tests := []struct {
		in      []string
		station utils.Coordinate
		bets    []bet
	}{

		{
			[]string{
				".#....#####...#..",
				"##...##.#####..##",
				"##...#...#.#####.",
				"..#.....#...###..",
				"..#.#.....#....##",
			},
			utils.Coordinate{X: 8, Y: 3},
			[]bet{
				{1, 801},
				{9, 1501},
				{36, 1403},
			},
		},
		{
			[]string{
				".#..##.###...#######",
				"##.############..##.",
				".#.######.########.#",
				".###.#######.####.#.",
				"#####.##.#.##.###.##",
				"..#####..#.#########",
				"####################",
				"#.####....###.#.#.##",
				"##.#################",
				"#####.##.###..####..",
				"..######..##.#######",
				"####.##.####...##..#",
				".#####..#.######.###",
				"##...#.##########...",
				"#.##########.#######",
				".####.#.###.###.#.##",
				"....##.##.###..#####",
				".#.#.###########.###",
				"#.#.#.#####.####.###",
				"###.##.####.##.#..##",
			},
			utils.Coordinate{X: 11, Y: 13},
			[]bet{
				{1, 1112},
				{2, 1201},
				{3, 1202},
				{10, 1208},
				{20, 1600},
				{50, 1609},
				{100, 1016},
				{199, 906},
				{200, 802},
				{201, 1009},
				{299, 1101},
				{300, -1},
			},
		},
	}

	for i, test := range tests {
		for _, b := range test.bets {
			actual := day10.CompleteVaporizationBet(test.in, test.station, b.bet)
			if actual != b.out {
				t.Errorf("CompleteVaporizationBet(%d, %d) => %d, want %d", i, b.bet, actual, b.out)
			}
		}
	}
}
