package day10

import (
	"sort"
	"strings"

	"advent-of-code-2019/pkg/utils"
)

const (
	asteroid = '#'
)

type void struct{}

func AsteroidMap(input []string) []utils.Coordinate {
	var belt []utils.Coordinate
	for y, line := range input {
		line = strings.TrimSpace(line)

		for x, r := range line {
			if r == asteroid {
				belt = append(belt, utils.Coordinate{X: x, Y: y})
			}
		}
	}
	return belt
}

func BestLocation(input []string) (utils.Coordinate, int) {
	belt := AsteroidMap(input)
	var bestCount int
	var best utils.Coordinate

	for _, current := range belt {
		angles := make(map[float64]void)
		var count int

		for _, target := range belt {
			if current == target {
				continue
			}

			angle := current.Angle(target)

			if _, ok := angles[angle]; !ok {
				angles[angle] = void{}
				count++
			}
		}

		if count > bestCount {
			bestCount = count
			best = current
		}
	}

	return best, bestCount
}

func CompleteVaporizationBet(input []string, station utils.Coordinate, bet int) int {
	belt := AsteroidMap(input)

	targets := make(map[float64]map[int]utils.Coordinate)
	var angles []float64

	for _, target := range belt {
		if target == station {
			continue
		}

		angle := station.Angle(target)
		distance := station.Distance(target)

		if _, ok := targets[angle]; !ok {
			targets[angle] = make(map[int]utils.Coordinate)
			angles = append(angles, angle)
		}
		targets[angle][distance] = target
	}

	sort.Float64s(angles)

	var hit int

	for len(angles) > 0 {
		for _, angle := range angles {
			closest := -1

			for distance := range targets[angle] {
				if distance < closest || closest == -1 {
					closest = distance
				}
			}

			hit++
			if hit == bet {
				return targets[angle][closest].X*100 + targets[angle][closest].Y
			}

			delete(targets[angle], closest)
		}

		var clean []float64
		for _, angle := range angles {
			if len(targets[angle]) > 0 {
				clean = append(clean, angle)
			}
		}
		angles = clean
	}

	return -1
}
