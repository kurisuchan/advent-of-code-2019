package day02

import (
	"fmt"
	"strconv"
	"strings"

	"advent-of-code-2019/pkg/utils"
)

const TARGET = 19690720

func Part1(intcode []int) int {
	return Program(intcode, 12, 2)

}

func Part2(intcode []int) int {
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			memory := utils.CopyIntSlice(intcode)

			if Program(memory, noun, verb) == TARGET {
				return 100*noun + verb
			}
		}
	}
	return -1
}

func ParseIntcode(input string) []int {
	input = strings.TrimSpace(input)
	steps := strings.Split(input, ",")
	var intcode []int
	for _, step := range steps {
		code, err := strconv.Atoi(step)
		if err != nil {
			panic(err)
		}
		intcode = append(intcode, code)
	}
	return intcode
}

func InstructionAdd(memory []int, pos int) int {
	memory[memory[pos+3]] = memory[memory[pos+1]] + memory[memory[pos+2]]
	return pos + 4
}

func InstructionMultiply(memory []int, pos int) int {
	memory[memory[pos+3]] = memory[memory[pos+1]] * memory[memory[pos+2]]
	return pos + 4
}

func Program(memory []int, noun, verb int) int {
	memory[1] = noun
	memory[2] = verb

	var pos int
	for {
		switch memory[pos] {
		case 1:
			pos = InstructionAdd(memory, pos)
		case 2:
			pos = InstructionMultiply(memory, pos)
		case 99:
			return memory[0]
		default:
			panic(fmt.Errorf("unknown intcode %d at position %d", memory[pos], pos))
		}
	}
}
