package day02_test

import (
	"reflect"
	"testing"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/utils"
)

func TestParseIntcode(t *testing.T) {
	tests := []struct {
		in  string
		out []int
	}{
		{"1,9,10,3,2,3,11,0,99,30,40,50\n", []int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50}},
	}

	for _, test := range tests {
		actual := day02.ParseIntcode(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("ParseIntcode(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestInstructionAdd(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{1, 0, 0, 0, 99}, []int{2, 0, 0, 0, 99}},
	}

	for _, test := range tests {
		actual := utils.CopyIntSlice(test.in)

		_ = day02.InstructionAdd(test.in, 0)
		if !reflect.DeepEqual(test.in, test.out) {
			t.Errorf("InstructionAdd(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestInstructionMultiply(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{2, 3, 0, 3, 99}, []int{2, 3, 0, 6, 99}},
		{[]int{2, 4, 4, 5, 99, 0}, []int{2, 4, 4, 5, 99, 9801}},
	}

	for _, test := range tests {
		actual := utils.CopyIntSlice(test.in)

		_ = day02.InstructionMultiply(test.in, 0)
		if !reflect.DeepEqual(test.in, test.out) {
			t.Errorf("InstructionMultiply(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestProgram(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50}, []int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50}},
		{[]int{1, 0, 0, 0, 99}, []int{2, 0, 0, 0, 99}},
		{[]int{2, 3, 0, 3, 99}, []int{2, 3, 0, 6, 99}},
		{[]int{2, 4, 4, 5, 99, 0}, []int{2, 4, 4, 5, 99, 9801}},
		{[]int{1, 1, 1, 4, 99, 5, 6, 0, 99}, []int{30, 1, 1, 4, 2, 5, 6, 0, 99}},
	}

	for _, test := range tests {
		actual := utils.CopyIntSlice(test.in)

		_ = day02.Program(actual, test.in[1], test.in[2])
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("Program(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}
