package day23

import (
	"advent-of-code-2019/pkg/day02"
)

type Message struct {
	To   int
	X, Y int
}

func Part1(input string) int {
	intCode := day02.ParseIntcode(input)
	nics := make(map[int]*Computer)
	buffer := make(map[int][]int)

	for id := 0; id < 50; id++ {
		nics[id] = NewComputer(intCode)
		nics[id].Input.Push(id)
	}

	for {
		for id, nic := range nics {
			out := nic.Run()
			if out == nil {
				continue
			}

			buffer[id] = append(buffer[id], *out)
			if len(buffer[id]) < 3 {
				continue
			}

			m := Message{
				To: buffer[id][0],
				X:  buffer[id][1],
				Y:  buffer[id][2],
			}
			buffer[id] = buffer[id][3:]

			if m.To == 255 {
				return m.Y

			} else {
				nics[m.To].Input.Push(m.X)
				nics[m.To].Input.Push(m.Y)
			}
		}
	}
}

func Part2(input string) int {
	intCode := day02.ParseIntcode(input)
	nics := make(map[int]*Computer)
	buffer := make(map[int][]int)

	for id := 0; id < 50; id++ {
		nics[id] = NewComputer(intCode)
		nics[id].Input.Push(id)
	}

	var nat Message
	var previousNat int

	for {
		for id, nic := range nics {
			out := nic.Run()
			if out == nil {
				continue
			}

			buffer[id] = append(buffer[id], *out)
			if len(buffer[id]) < 3 {
				continue
			}

			m := Message{
				To: buffer[id][0],
				X:  buffer[id][1],
				Y:  buffer[id][2],
			}
			buffer[id] = buffer[id][3:]

			if m.To == 255 {
				nat = m

			} else {
				nics[m.To].Input.Push(m.X)
				nics[m.To].Input.Push(m.Y)
			}
		}

		allIdle := true
		for _, nic := range nics {
			if !nic.Idle {
				allIdle = false
			}
		}

		if allIdle && nat.To == 255 {
			if previousNat == nat.Y {
				return nat.Y
			}
			previousNat = nat.Y

			nics[0].Input.Push(nat.X)
			nics[0].Input.Push(nat.Y)
		}
	}
}
