package day23

import (
	"fmt"

	"advent-of-code-2019/pkg/day09"
)

type Computer struct {
	Pos    int
	Memory map[int]int
	Input  *Queue
	Done   bool
	Idle   bool
}

func NewComputer(intCode []int) *Computer {
	c := &Computer{
		Input:  new(Queue),
		Memory: make(map[int]int),
	}

	for i, v := range intCode {
		c.Memory[i] = v
	}

	return c
}

func (c *Computer) Run() *int {
	var mask, output, offset int

	c.Idle = false

	for !c.Done {
		mask = c.Memory[c.Pos] / 100

		switch c.Memory[c.Pos] % 100 {
		case day09.OpcodeAdd:
			c.Pos = day09.InstructionAdd(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeMultiply:
			c.Pos = day09.InstructionMultiply(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeInput:
			msg, ok := c.Input.Pop()

			c.Idle = !ok
			c.Pos = day09.InstructionInput(c.Memory, c.Pos, mask, offset, msg)
			return nil

		case day09.OpcodeOutput:
			c.Pos, output = day09.InstructionOutput(c.Memory, c.Pos, mask, offset)
			return &output

		case day09.OpcodeJumpIfTrue:
			c.Pos = day09.InstructionJumpIfTrue(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeJumpIfFalse:
			c.Pos = day09.InstructionJumpIfFalse(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeLessThan:
			c.Pos = day09.InstructionLessThan(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeEqual:
			c.Pos = day09.InstructionEqual(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeOffset:
			c.Pos, offset = day09.InstructionOffset(c.Memory, c.Pos, mask, offset)

		case day09.OpcodeExit:
			c.Done = true
			return nil

		default:
			panic(fmt.Errorf("unknown intcode %d at c.Position %d", c.Memory[c.Pos], c.Pos))
		}
	}

	return nil
}
