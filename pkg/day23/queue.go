package day23

type Queue struct {
	Items []int
}

func (q *Queue) Push(item int) {
	q.Items = append(q.Items, item)
}

func (q *Queue) Pop() (int, bool) {
	if q.Len() == 0 {
		return -1, false
	}

	item := q.Items[0]
	q.Items = q.Items[1:]
	return item, true
}

func (q *Queue) Len() int {
	return len(q.Items)
}
