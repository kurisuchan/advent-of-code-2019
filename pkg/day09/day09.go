package day09

import (
	"fmt"

	"advent-of-code-2019/pkg/day02"
)

func BoostCode(input string) int {
	out := RunProgram(input, 1)
	if out[0] < 5 {
		panic(fmt.Sprintf("diagnostics failed at instruction %d", out[0]))
	}
	return out[0]
}

func Coordinates(input string) int {
	out := RunProgram(input, 2)
	return out[0]
}

func RunProgram(code string, input ...int) []int {
	var result []int
	in := make(chan int)
	out := make(chan int)
	intCode := day02.ParseIntcode(code)

	go Computer(intCode, in, out)

	go func() {
		for _, v := range input {
			in <- v
		}
	}()

	for o := range out {
		result = append(result, o)
	}

	return result
}
