package day09

import "fmt"

const (
	OpcodeAdd = iota + 1
	OpcodeMultiply
	OpcodeInput
	OpcodeOutput
	OpcodeJumpIfTrue
	OpcodeJumpIfFalse
	OpcodeLessThan
	OpcodeEqual
	OpcodeOffset
	OpcodeExit = 99
)

const (
	ModePosition = iota
	ModeImmediate
	ModeOffset
)

func Computer(intCode []int, in <-chan int, out chan<- int) {
	var pos, mask, output, offset int

	memory := make(map[int]int)
	for i, v := range intCode {
		memory[i] = v
	}

	for {
		mask = memory[pos] / 100

		switch memory[pos] % 100 {
		case OpcodeAdd:
			pos = InstructionAdd(memory, pos, mask, offset)
		case OpcodeMultiply:
			pos = InstructionMultiply(memory, pos, mask, offset)
		case OpcodeInput:
			pos = InstructionInput(memory, pos, mask, offset, <-in)
		case OpcodeOutput:
			pos, output = InstructionOutput(memory, pos, mask, offset)
			out <- output
		case OpcodeJumpIfTrue:
			pos = InstructionJumpIfTrue(memory, pos, mask, offset)
		case OpcodeJumpIfFalse:
			pos = InstructionJumpIfFalse(memory, pos, mask, offset)
		case OpcodeLessThan:
			pos = InstructionLessThan(memory, pos, mask, offset)
		case OpcodeEqual:
			pos = InstructionEqual(memory, pos, mask, offset)
		case OpcodeOffset:
			pos, offset = InstructionOffset(memory, pos, mask, offset)
		case OpcodeExit:
			close(out)
			return
		default:
			panic(fmt.Errorf("unknown intcode %d at position %d", memory[pos], pos))
		}
	}
}

func Read(memory map[int]int, pos, mask, offset, arguments int) []int {
	var out []int

	for i := 1; i <= arguments; i++ {
		switch mask % 10 {
		case ModePosition:
			out = append(out, memory[pos+i])
		case ModeImmediate:
			out = append(out, pos+i)
		case ModeOffset:
			out = append(out, memory[pos+i]+offset)
		default:
			panic(fmt.Errorf("unknown mode: %d", i%10))
		}
		mask = mask / 10
	}
	return out
}

func InstructionAdd(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 3)
	memory[ptrs[2]] = memory[ptrs[0]] + memory[ptrs[1]]

	return pos + 4
}

func InstructionMultiply(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 3)
	memory[ptrs[2]] = memory[ptrs[0]] * memory[ptrs[1]]

	return pos + 4
}

func InstructionInput(memory map[int]int, pos, mask, offset, value int) int {
	ptrs := Read(memory, pos, mask, offset, 1)
	memory[ptrs[0]] = value

	return pos + 2
}

func InstructionOutput(memory map[int]int, pos, mask, offset int) (int, int) {
	ptrs := Read(memory, pos, mask, offset, 1)
	return pos + 2, memory[ptrs[0]]
}

func InstructionJumpIfTrue(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 2)
	if memory[ptrs[0]] != 0 {
		return memory[ptrs[1]]
	}
	return pos + 3
}

func InstructionJumpIfFalse(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 2)
	if memory[ptrs[0]] == 0 {
		return memory[ptrs[1]]
	}
	return pos + 3
}

func InstructionLessThan(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 3)
	if memory[ptrs[0]] < memory[ptrs[1]] {
		memory[ptrs[2]] = 1
	} else {
		memory[ptrs[2]] = 0
	}
	return pos + 4
}

func InstructionEqual(memory map[int]int, pos, mask, offset int) int {
	ptrs := Read(memory, pos, mask, offset, 3)

	if memory[ptrs[0]] == memory[ptrs[1]] {
		memory[ptrs[2]] = 1
	} else {
		memory[ptrs[2]] = 0
	}

	return pos + 4
}

func InstructionOffset(memory map[int]int, pos, mask, offset int) (int, int) {
	ptrs := Read(memory, pos, mask, offset, 1)
	return pos + 2, offset + memory[ptrs[0]]
}

