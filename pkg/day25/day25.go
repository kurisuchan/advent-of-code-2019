package day25

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
)

func Adventure(input string) int {
	intCode := day02.ParseIntcode(input)
	in := make(chan int)
	out := make(chan int)
	go day09.Computer(intCode, in, out)

	go func() {
		reader := bufio.NewScanner(os.Stdin)
		for reader.Scan() {
			for _, r := range reader.Bytes() {
				in <- int(r)
			}
			in <- int('\n')
		}
	}()

	var buf strings.Builder
	for o := range out {
		if o == '\n' {
			fmt.Println(buf.String())
			buf.Reset()
		} else if o >= 0 && o <= unicode.MaxASCII {
			buf.WriteByte(byte(o))
		} else {
			fmt.Println(buf.String())
			return o
		}
	}
	return -1
}
