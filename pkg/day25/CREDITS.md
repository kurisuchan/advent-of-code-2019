You use all *fifty stars* to align the warp drive! Suddenly, the Sun starts getting a lot closer.

You'll be home before you know it.

> Congratulations! You've finished every puzzle in Advent of Code 2019! I hope you had as much fun solving them[^1] as I
> had making them for you. I'd love to hear about your adventure; you can get in touch with me via contact info on
> [my website](http://was.tl/) or through [Twitter](https://twitter.com/ericwastl).
> 
> If you'd like to see more things like this in the future, please consider
> [supporting](https://adventofcode.com/2019/support)[^1] Advent of Code and sharing it with others.
> 
> To hear about future projects, you can follow me on [Twitter](https://twitter.com/ericwastl).
> 
> I've highlighted[^2] the easter eggs in each puzzle, just in case you missed any. Hover your mouse over them, and the easter egg will appear.


[^1]: I did!
[^2]: Yep, just like that.  There's at least one in the description for each day.
