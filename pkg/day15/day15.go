package day15

import (
	"strings"
	"time"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/day09"
	"advent-of-code-2019/pkg/utils"
)

const (
	Wall int = iota
	Moved
	Found
)

const (
	MapUnknown int = iota
	MapWall
	MapEmpty
	MapOxygenSystem
	MapDeadEnd
)

const (
	North int = iota + 1
	South
	West
	East
)

type Map map[utils.Coordinate]int

func FindOxygenSystem(grid Map) int {
	for pos, what := range grid {
		if what == MapOxygenSystem {
			return grid.Distance(utils.Coordinate{}, pos)
		}
	}

	return 0
}

func TimeToFill(grid Map) time.Duration {
	distances := make(map[utils.Coordinate]int)
	var oxygenSystemPosition utils.Coordinate

	for pos, what := range grid {
		if what == MapOxygenSystem {
			oxygenSystemPosition = pos
			break
		}
	}
	grid.Flood(distances, oxygenSystemPosition, 0)

	var maxDistance int
	for _, distance := range distances {
		maxDistance = utils.MaxInt(distance, maxDistance)
	}

	return time.Duration(maxDistance) * time.Minute
}

func Explore(input string) Map {
	intCode := day02.ParseIntcode(input)

	in := make(chan int)
	out := make(chan int, 1)
	go day09.Computer(intCode, in, out)

	var position, oxygenSystem utils.Coordinate
	grid := make(Map)
	grid[position] = MapEmpty
	directions := []utils.Coordinate{
		{},
		{X: 0, Y: -1},
		{X: 0, Y: 1},
		{X: -1, Y: 0},
		{X: 1, Y: 0},
	}
	turns := []int{North, East, South, West}
	direction := North

	in <- turns[direction]

	for response := range out {
		// attempted to move to
		target := utils.Coordinate{
			X: position.X + directions[turns[direction]].X,
			Y: position.Y + directions[turns[direction]].Y,
		}

		// update map if not already visited
		if _, ok := grid[target]; !ok {
			grid[target] = response + 1
		}

		// count number of walls and dead ends to see if this path is a dead end or leads to one
		var wallCount int
		for _, d := range turns {
			if grid[position.Move(directions[d])] == MapWall || grid[position.Move(directions[d])] == MapDeadEnd {
				wallCount++
			}
		}

		// if we can only move in one (or less) directions, this is a (path leading to a) dead end
		if wallCount >= 3 {
			grid[position] = MapDeadEnd
		}

		switch response {
		case Wall:
		case Found:
			oxygenSystem = target
			fallthrough
		case Moved:
			position = target
		}

		// prefer to move to an unexplored location
		var choiceMade bool
		for d, newDirection := range turns {
			at := grid[position.Move(directions[newDirection])]
			if at == MapUnknown {
				direction = d
				choiceMade = true
			}
		}

		// no unexplored locations available, move to an empty space. dead end paths are ignored
		if !choiceMade {
			for d, newDirection := range turns {
				at := grid[position.Move(directions[newDirection])]
				if at == MapEmpty {
					direction = d
					choiceMade = true
				}
			}
		}

		// no more valid steps
		if !choiceMade {
			in <- 0
			continue
		}

		in <- turns[direction]
	}

	// clean up map
	for c, w := range grid {
		if w == MapDeadEnd {
			grid[c] = MapEmpty
		}
	}
	grid[oxygenSystem] = MapOxygenSystem

	return grid
}

func (m Map) String() string {
	var minX, minY, maxX, maxY int
	for c := range m {
		minX = utils.MinInt(minX, c.X)
		minY = utils.MinInt(minY, c.Y)
		maxX = utils.MaxInt(maxX, c.X)
		maxY = utils.MaxInt(maxY, c.Y)
	}

	var out strings.Builder

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			if x == 0 && y == 0 {
				out.WriteString("S")
				continue
			}
			c := utils.Coordinate{X: x, Y: y}
			switch m[c] {
			case MapWall:
				out.WriteString("█")
			case MapEmpty:
				out.WriteString(" ")
			case MapOxygenSystem:
				out.WriteString("O")
			default:
				out.WriteString("░")
			}
		}
		out.WriteString("\n")
	}
	return out.String()
}

func (m Map) Distance(from, to utils.Coordinate) int {
	grid := make(map[utils.Coordinate]int)
	m.Flood(grid, from, 0)
	return grid[to]
}

func (m Map) Flood(grid map[utils.Coordinate]int, pos utils.Coordinate, steps int) {
	directions := []utils.Coordinate{
		{X: 0, Y: -1},
		{X: 0, Y: 1},
		{X: -1, Y: 0},
		{X: 1, Y: 0},
	}
	if m[pos] == MapWall {
		return
	}
	if _, ok := grid[pos]; ok {
		return
	}
	grid[pos] = steps

	for _, d := range directions {
		m.Flood(grid, pos.Move(d), steps+1)
	}
}
