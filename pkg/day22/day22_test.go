package day22_test

import (
	"reflect"
	"testing"

	"advent-of-code-2019/pkg/day22"
)

func TestRunInstructions(t *testing.T) {
	tests := []struct {
		in   []string
		size int
		out  []int
	}{
		{
			[]string{
				"deal with increment 7",
				"deal into new stack",
				"deal into new stack",
			},
			10,
			[]int{0, 3, 6, 9, 2, 5, 8, 1, 4, 7},
		},
		{
			[]string{
				"cut 6",
				"deal with increment 7",
				"deal into new stack",
			},
			10,
			[]int{3, 0, 7, 4, 1, 8, 5, 2, 9, 6},
		},
		{
			[]string{
				"deal with increment 7",
				"deal with increment 9",
				"cut -2",
			},
			10,
			[]int{6, 3, 0, 7, 4, 1, 8, 5, 2, 9},
		},
		{
			[]string{
				"deal into new stack",
				"cut -2",
				"deal with increment 7",
				"cut 8",
				"cut -4",
				"deal with increment 7",
				"cut 3",
				"deal with increment 9",
				"deal with increment 3",
				"cut -1",
			},
			10,
			[]int{9, 2, 5, 8, 1, 4, 7, 0, 3, 6},
		},
	}

	for i, test := range tests {
		actual := day22.RunInstructions(day22.MakeDeck(test.size), test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("RunInstructions(#%d) => %v, want %v", i, actual, test.out)
		}
	}
}

func TestDealNewStack(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, []int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}},
	}

	for _, test := range tests {
		actual := day22.DealNewStack(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("DealNewStack(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestCutCards(t *testing.T) {
	tests := []struct {
		in  []int
		n   int
		out []int
	}{
		{[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, []int{3, 4, 5, 6, 7, 8, 9, 0, 1, 2}},
		{[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, -4, []int{6, 7, 8, 9, 0, 1, 2, 3, 4, 5}},
	}

	for _, test := range tests {
		actual := day22.CutCards(test.in, test.n)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("CutCards(%v, %d) => %v, want %v", test.in, test.n, actual, test.out)
		}
	}
}

func TestDealWithIncrement(t *testing.T) {
	tests := []struct {
		in  []int
		n   int
		out []int
	}{
		{[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, []int{0, 7, 4, 1, 8, 5, 2, 9, 6, 3,}},
	}

	for _, test := range tests {
		actual := day22.DealWithIncrement(test.in, test.n)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("DealWithIncrement(%v, %d) => %v, want %v", test.in, test.n, actual, test.out)
		}
	}
}
