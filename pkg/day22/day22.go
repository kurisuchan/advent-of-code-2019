package day22

import (
	"math/big"
	"regexp"
	"strings"

	"advent-of-code-2019/pkg/utils"
)

func Part1(input []string) int {
	deck := MakeDeck(10007)
	deck = RunInstructions(deck, input)

	for i, card := range deck {
		if card == 2019 {
			return i
		}
	}
	return -1
}

type Instruction struct {
	Id        uint8
	Parameter int
}

const (
	InsDealNewStack uint8 = iota
	InsCutCards
	InsDealWithIncrement
)

func ParseInstructions(input []string) []Instruction {
	var instructions []Instruction
	digits := regexp.MustCompile(`(-?\d+)`)

	for _, line := range input {
		d := digits.FindAllString(line, 1)

		if strings.HasPrefix(line, "cut") {
			instructions = append(instructions, Instruction{Id: InsCutCards, Parameter: utils.MustInt(d[0])})

		} else if strings.HasPrefix(line, "deal with increment") {
			instructions = append(instructions, Instruction{Id: InsDealWithIncrement, Parameter: utils.MustInt(d[0])})

		} else if strings.HasPrefix(line, "deal into new stack") {
			instructions = append(instructions, Instruction{Id: InsDealNewStack})
		}
	}

	return instructions
}

func MakeDeck(size int) []int {
	deck := make([]int, size)
	for i := 0; i < size; i++ {
		deck[i] = i
	}
	return deck
}

func DealNewStack(deck []int) []int {
	size := len(deck)
	newDeck := make([]int, size)
	for i, card := range deck {
		newDeck[size-i-1] = card
	}
	return newDeck
}

func CutCards(deck []int, n int) []int {
	if n > 0 {
		cutCards, newDeck := deck[:n], deck[n:]
		newDeck = append(newDeck, cutCards...)
		return newDeck
	} else {
		size := len(deck)
		newDeck, cutCards := deck[size+n:], deck[:size+n]
		newDeck = append(newDeck, cutCards...)
		return newDeck
	}
}

func DealWithIncrement(deck []int, n int) []int {
	size := len(deck)
	newDeck := make([]int, size)
	for i, card := range deck {
		newDeck[(i*n)%size] = card
	}

	return newDeck
}

func RunInstructions(deck []int, input []string) []int {
	instructions := ParseInstructions(input)
	for _, ins := range instructions {
		switch ins.Id {
		case InsDealNewStack:
			deck = DealNewStack(deck)
		case InsCutCards:
			deck = CutCards(deck, ins.Parameter)
		case InsDealWithIncrement:
			deck = DealWithIncrement(deck, ins.Parameter)
		}
	}
	return deck
}

// Part 2 maths nonsense.
// If you're not a mathemagician, find someone who is and blindly reimplement their sorcery in your own words.
// All credit for the magic goes to [Kunal](https://reddit.com/user/knl_).
func Part2(input []string) int {
	ins := ParseInstructions(input)

	const (
		loopCount   int64 = 101741582076661
		deckSize    int64 = 119315717514047
		cardPostion int64 = 2020
	)

	return int(CalculateResult(cardPostion, loopCount, deckSize, ins))
}

func CalculateResult(cardPosition, loopCount, deckSize int64, instructions []Instruction) int64 {
	offset, multiplier := GetParameters(deckSize, loopCount, instructions)

	multiplier.ModInverse(multiplier, big.NewInt(deckSize))

	offset.Add(offset.Neg(offset), big.NewInt(cardPosition))
	offset.Mul(offset, multiplier)
	offset.Mod(offset, big.NewInt(deckSize))

	return offset.Int64()
}

func GetParameters(deckSize, iteration int64, instructions []Instruction) (*big.Int, *big.Int) {
	multiplier := big.NewInt(1)
	offset := big.NewInt(0)

	bigDeck := big.NewInt(deckSize)
	bigIter := big.NewInt(iteration)
	big1 := big.NewInt(1)

	for _, ins := range instructions {
		param := big.NewInt(int64(ins.Parameter))

		switch ins.Id {
		case InsDealNewStack:
			offset.Sub(big.NewInt(deckSize-1), offset)
			offset.Mod(offset, bigDeck)

			multiplier.Neg(multiplier)
			multiplier.Mod(multiplier, bigDeck)
		case InsCutCards:
			offset.Sub(offset, param)
			offset.Mod(offset, bigDeck)
		case InsDealWithIncrement:
			offset.Mul(offset, param)
			offset.Mod(offset, bigDeck)

			multiplier.Mul(multiplier, param)
			multiplier.Mod(multiplier, bigDeck)
		}
	}

	nmul := new(big.Int).Exp(multiplier, bigIter, bigDeck)
	nmul.Mod(nmul, bigDeck)

	noff := new(big.Int).Exp(multiplier, bigIter, bigDeck)
	noff.Sub(noff, big1)

	noff.Mul(noff, new(big.Int).ModInverse(new(big.Int).Sub(multiplier, big1), bigDeck))
	noff.Mul(noff, offset)
	noff.Mod(noff, bigDeck)

	return noff, nmul
}
