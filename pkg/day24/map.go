package day24

import (
	"strings"

	"advent-of-code-2019/pkg/utils"
)

const (
	squareSize   = 5
	squareMax    = squareSize - 1
	squareCenter = squareSize / 2
)

var (
	center = utils.Coordinate{X: 2, Y: 2}
	bug    = struct{}{}
)

type Map map[utils.Coordinate]struct{}

func ParseInput(input []string) Map {
	grid := make(Map)

	for y, row := range input {
		for x, r := range row {
			if r == '#' {
				grid[utils.Coordinate{X: x, Y: y}] = bug
			}
		}
	}
	return grid
}

func (m Map) BioDiversity() int {
	var rating int
	for c := range m {
		rating += 1 << uint((squareSize*c.Y)+c.X)
	}
	return rating
}

// BugCount counts the amount of bugs in a rectangle x1 <= x2, y1 <= y2
func (m Map) BugCount(x1, y1 int, x2, y2 int) int {
	var bugCount int
	for y := utils.MinInt(y1, y2); y <= utils.MaxInt(y1, y2); y++ {
		for x := utils.MinInt(x1, x2); x <= utils.MaxInt(x1, x2); x++ {
			if m.HasBugAt(x, y) {
				bugCount++
			}
		}
	}
	return bugCount
}

func (m Map) HasBugAt(x, y int) bool {
	_, ok := m[utils.Coordinate{X: x, Y: y}]
	return ok
}

func (m Map) Grow(inner, outer Map) Map {
	grid := make(Map)

	for y := 0; y < squareSize; y++ {
		for x := 0; x < squareSize; x++ {
			pos := utils.Coordinate{X: x, Y: y}

			// only skip center on a recursive map
			if pos == center && (inner != nil || outer != nil) {
				continue
			}

			var bugCount int

			// center of this grid contains another grid
			if inner != nil {
				switch {
				case x == squareCenter && y == squareCenter-1:
					bugCount += inner.BugCount(0, 0, squareMax, 0)

				case x == squareCenter && y == squareCenter+1:
					bugCount += inner.BugCount(0, squareMax, squareMax, squareMax)

				case x == squareCenter-1 && y == squareCenter:
					bugCount += inner.BugCount(0, 0, 0, squareMax)

				case x == squareCenter+1 && y == squareCenter:
					bugCount += inner.BugCount(squareMax, 0, squareMax, squareMax)
				}
			}

			// outside of this grid is another grid
			if outer != nil {
				if x == 0 && outer.HasBugAt(squareCenter-1, squareCenter) {
					bugCount++
				} else if x == squareMax && outer.HasBugAt(squareCenter+1, squareCenter) {
					bugCount++
				}

				if y == 0 && outer.HasBugAt(squareCenter, squareCenter-1) {
					bugCount++
				} else if y == squareMax && outer.HasBugAt(squareCenter, squareCenter+1) {
					bugCount++
				}
			}

			for _, d := range utils.Cardinals {
				if m.HasBugAt(pos.X+d.X, pos.Y+d.Y) {
					bugCount++
				}
			}

			if bugCount == 1 || (bugCount == 2 && !m.HasBugAt(pos.X, pos.Y)) {
				grid[pos] = bug
			}
		}
	}
	return grid
}

func (m Map) String() string {
	var out strings.Builder
	for y := 0; y < 5; y++ {
		for x := 0; x < 5; x++ {
			if _, ok := m[utils.Coordinate{X: x, Y: y}]; ok {
				out.WriteString("#")
			} else {
				out.WriteString(".")
			}
		}
		out.WriteString("\n")
	}
	return out.String()
}
