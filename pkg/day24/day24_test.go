package day24_test

import (
	"testing"

	"advent-of-code-2019/pkg/day24"
)

func TestBugs(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{
			[]string{
				"....#",
				"#..#.",
				"#..##",
				"..#..",
				"#....",
			},
			2129920,
		},
	}

	for i, test := range tests {
		actual := day24.Bugs(test.in)
		if actual != test.out {
			t.Errorf("Bugs(#%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestRecursiveBugs(t *testing.T) {
	tests := []struct {
		in       []string
		duration int
		out      int
	}{
		{
			[]string{
				"....#",
				"#..#.",
				"#.?##",
				"..#..",
				"#....",
			},
			10,
			99,
		},
	}

	for i, test := range tests {
		actual := day24.RecursiveBugs(test.in, test.duration)
		if actual != test.out {
			t.Errorf("Bugs(#%d) => %d, want %d", i, actual, test.out)
		}
	}
}
