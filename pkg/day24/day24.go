package day24

func Bugs(input []string) int {
	grid := ParseInput(input)
	rating := grid.BioDiversity()

	previous := map[int]struct{}{
		rating: {},
	}

	for {
		grid = grid.Grow(nil, nil)
		rating = grid.BioDiversity()

		if _, ok := previous[rating]; ok {
			break
		}
		previous[rating] = struct{}{}
	}

	return grid.BioDiversity()
}

func RecursiveBugs(input []string, duration int) int {
	grids := make(RecursiveMap)
	grids[0] = ParseInput(input)

	for i := 0; i < duration; i++ {
		grids = grids.Grow()
	}

	var bugCount int
	for _, m := range grids {
		bugCount += len(m)
	}

	return bugCount
}
