package day24_test

import (
	"testing"

	"advent-of-code-2019/pkg/day24"
)

func TestMap_BioDiversity(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{
			[]string{
				".....",
				".....",
				".....",
				"#....",
				".#...",
			},
			2129920,
		},
	}

	for i, test := range tests {
		m := day24.ParseInput(test.in)
		actual := m.BioDiversity()
		if actual != test.out {
			t.Errorf("Map.BioDiversity(#%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestMap_Grow(t *testing.T) {
	tests := []struct {
		in  []string
		out []string
	}{
		{
			[]string{
				"....#",
				"#..#.",
				"#..##",
				"..#..",
				"#....",
			},
			[]string{
				"#..#.",
				"####.",
				"###.#",
				"##.##",
				".##..",
			},
		},
		{
			[]string{
				"#..#.",
				"####.",
				"###.#",
				"##.##",
				".##..",
			},
			[]string{
				"#####",
				"....#",
				"....#",
				"...#.",
				"#.###",
			},
		},
		{
			[]string{
				"#####",
				"....#",
				"....#",
				"...#.",
				"#.###",
			},
			[]string{
				"#....",
				"####.",
				"...##",
				"#.##.",
				".##.#",
			},
		},
		{
			[]string{
				"#....",
				"####.",
				"...##",
				"#.##.",
				".##.#",
			},
			[]string{
				"####.",
				"....#",
				"##..#",
				".....",
				"##...",
			},
		},
	}

	for i, test := range tests {
		m := day24.ParseInput(test.in)
		out := day24.ParseInput(test.out)

		actual := m.Grow(nil, nil)
		if actual.BioDiversity() != out.BioDiversity() {
			t.Errorf("Map.Grow(#%d) => %s, want %s", i, actual.String(), out.String())
		}
	}
}

// yay test coverage
func TestMap_String(t *testing.T) {
	tests := []struct {
		in  []string
		out string
	}{
		{
			[]string{
				"....#",
				"#..#.",
				"#..##",
				"..#..",
				"#....",
			},
			"....#\n#..#.\n#..##\n..#..\n#....\n",
		},
	}

	for i, test := range tests {
		m := day24.ParseInput(test.in)

		actual := m.String()
		if actual != test.out {
			t.Errorf("Map.String(#%d) =>\n%s\nwant\n%s", i, actual, test.out)
		}
	}
}
