package day24

import "advent-of-code-2019/pkg/utils"

type RecursiveMap map[int]Map

func (rm RecursiveMap) Grow() RecursiveMap {
	newGrid := make(RecursiveMap)
	var outerLevel, innerLevel int

	for l, m := range rm {
		newGrid[l] = m.Grow(rm[l+1], rm[l-1])

		outerLevel = utils.MinInt(outerLevel, l)
		innerLevel = utils.MaxInt(innerLevel, l)
	}

	newGrid[outerLevel-1] = make(Map).Grow(rm[outerLevel], nil)
	if len(newGrid[outerLevel-1]) == 0 {
		delete(newGrid, outerLevel-1)
	}

	newGrid[innerLevel+1] = make(Map).Grow(nil, rm[innerLevel])
	if len(newGrid[innerLevel+1]) == 0 {
		delete(newGrid, innerLevel+1)
	}
	return newGrid
}
