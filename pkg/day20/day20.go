package day20

import (
	"fmt"

	"advent-of-code-2019/pkg/utils"
)

const maxRecursion = 50

func DonutMaze(input []string) int {
	start, goal := ParseMap(input)
	return start.Path(goal, false)
}

func RecursiveMaze(input []string) int {
	start, goal := ParseMap(input)
	return start.Path(goal, true)
}

type Node struct {
	Name      rune
	Neighbors []*Edge
}

type Edge struct {
	Name      string
	Direction int
	Node      *Node
}

func ParseMap(input []string) (*Node, *Node) {
	grid := make(map[utils.Coordinate]*Node)
	var maxX, maxY int
	for y, row := range input {
		for x, r := range row {
			if r == '#' || r == ' ' {
				continue
			}
			maxX = utils.MaxInt(maxX, x)
			maxY = utils.MaxInt(maxY, y)
			n := &Node{Name: r}
			grid[utils.Coordinate{X: x, Y: y}] = n
		}
	}

	portals := make(map[string]*Node)
	for c, node := range grid {
		if node.Name == '.' {
			for _, direction := range utils.Cardinals {
				posA := c.Move(direction)
				if nodeA, ok := grid[posA]; ok && nodeA.Name != '.' {
					posB := posA.Move(direction)
					if nodeB, ok := grid[posB]; ok && nodeB.Name != '.' {
						var portalId string
						if posA.Y < posB.Y || posA.X < posB.X {
							portalId = fmt.Sprintf("%c%c", nodeA.Name, nodeB.Name)
						} else {
							portalId = fmt.Sprintf("%c%c", nodeB.Name, nodeA.Name)
						}

						if target, ok := portals[portalId]; ok {
							outwardPortal := &Edge{Name: portalId, Direction: -1}
							inwardPortal := &Edge{Name: portalId, Direction: 1}

							if maxX-c.X <= 3 || maxY-c.Y <= 3 || c.X <= 3 || c.Y <= 3 {
								outwardPortal.Node = target
								node.Neighbors = append(node.Neighbors, outwardPortal)

								inwardPortal.Node = node
								target.Neighbors = append(target.Neighbors, inwardPortal)
							} else {
								outwardPortal.Node = node
								target.Neighbors = append(target.Neighbors, outwardPortal)

								inwardPortal.Node = target
								node.Neighbors = append(node.Neighbors, inwardPortal)
							}

							delete(portals, portalId)
						} else {
							portals[portalId] = node
						}
					}
				}
			}
		}
	}

	for c, r := range grid {
		for _, d := range utils.Cardinals {
			n := c.Move(d)
			if x, ok := grid[n]; ok {
				if x.Name == '.' {
					r.Neighbors = append(r.Neighbors, &Edge{Node: x})
				}
			}
		}
	}

	return portals["AA"], portals["ZZ"]
}

type Cache struct {
	Node  *Node
	Level int
}

func (n *Node) Path(target *Node, recursive bool) int {
	visited := map[Cache]bool{
		Cache{Node: n, Level: 0}: true,
	}
	frontier := utils.NewQueue()

	frontier.Set(Cache{Node: n, Level: 0}, 0)

	for !frontier.IsEmpty() {
		pos, priority := frontier.Pop()
		cPos := pos.(Cache)

		if cPos.Node == target && cPos.Level == 0 {
			return priority
		}

		for _, neighbor := range cPos.Node.Neighbors {
			nc := Cache{Node: neighbor.Node, Level: cPos.Level}
			if recursive {
				nc.Level += neighbor.Direction
			}

			if _, ok := visited[nc]; ok || nc.Level < 0 || nc.Level > maxRecursion {
				continue
			}

			visited[nc] = true

			if cost, ok := frontier.Check(nc); !ok {
				frontier.Set(nc, priority+1)
			} else {
				if priority+1 < cost {
					frontier.Set(nc, priority+1)
				}
			}

		}
	}
	return -1
}
