package day20_test

import (
	"testing"

	"advent-of-code-2019/pkg/day20"
)

func TestDonutMaze(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"         A           ",
			"         A           ",
			"  #######.#########  ",
			"  #######.........#  ",
			"  #######.#######.#  ",
			"  #######.#######.#  ",
			"  #######.#######.#  ",
			"  #####  B    ###.#  ",
			"BC...##  C    ###.#  ",
			"  ##.##       ###.#  ",
			"  ##...DE  F  ###.#  ",
			"  #####    G  ###.#  ",
			"  #########.#####.#  ",
			"DE..#######...###.#  ",
			"  #.#########.###.#  ",
			"FG..#########.....#  ",
			"  ###########.#####  ",
			"             Z       ",
			"             Z       ",
		}, 23},
		{[]string{
			"                   A               ",
			"                   A               ",
			"  #################.#############  ",
			"  #.#...#...................#.#.#  ",
			"  #.#.#.###.###.###.#########.#.#  ",
			"  #.#.#.......#...#.....#.#.#...#  ",
			"  #.#########.###.#####.#.#.###.#  ",
			"  #.............#.#.....#.......#  ",
			"  ###.###########.###.#####.#.#.#  ",
			"  #.....#        A   C    #.#.#.#  ",
			"  #######        S   P    #####.#  ",
			"  #.#...#                 #......VT",
			"  #.#.#.#                 #.#####  ",
			"  #...#.#               YN....#.#  ",
			"  #.###.#                 #####.#  ",
			"DI....#.#                 #.....#  ",
			"  #####.#                 #.###.#  ",
			"ZZ......#               QG....#..AS",
			"  ###.###                 #######  ",
			"JO..#.#.#                 #.....#  ",
			"  #.#.#.#                 ###.#.#  ",
			"  #...#..DI             BU....#..LF",
			"  #####.#                 #.#####  ",
			"YN......#               VT..#....QG",
			"  #.###.#                 #.###.#  ",
			"  #.#...#                 #.....#  ",
			"  ###.###    J L     J    #.#.###  ",
			"  #.....#    O F     P    #.#...#  ",
			"  #.###.#####.#.#####.#####.###.#  ",
			"  #...#.#.#...#.....#.....#.#...#  ",
			"  #.#####.###.###.#.#.#########.#  ",
			"  #...#.#.....#...#.#.#.#.....#.#  ",
			"  #.###.#####.###.###.#.#.#######  ",
			"  #.#.........#...#.............#  ",
			"  #########.###.###.#############  ",
			"           B   J   C               ",
			"           U   P   P               ",
		}, 58},
	}

	for i, test := range tests {
		actual := day20.DonutMaze(test.in)
		if actual != test.out {
			t.Errorf("DonutMaze(%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestRecursiveMaze(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"         A           ",
			"         A           ",
			"  #######.#########  ",
			"  #######.........#  ",
			"  #######.#######.#  ",
			"  #######.#######.#  ",
			"  #######.#######.#  ",
			"  #####  B    ###.#  ",
			"BC...##  C    ###.#  ",
			"  ##.##       ###.#  ",
			"  ##...DE  F  ###.#  ",
			"  #####    G  ###.#  ",
			"  #########.#####.#  ",
			"DE..#######...###.#  ",
			"  #.#########.###.#  ",
			"FG..#########.....#  ",
			"  ###########.#####  ",
			"             Z       ",
			"             Z       ",
		}, 26},
		{[]string{
			"                   A               ",
			"                   A               ",
			"  #################.#############  ",
			"  #.#...#...................#.#.#  ",
			"  #.#.#.###.###.###.#########.#.#  ",
			"  #.#.#.......#...#.....#.#.#...#  ",
			"  #.#########.###.#####.#.#.###.#  ",
			"  #.............#.#.....#.......#  ",
			"  ###.###########.###.#####.#.#.#  ",
			"  #.....#        A   C    #.#.#.#  ",
			"  #######        S   P    #####.#  ",
			"  #.#...#                 #......VT",
			"  #.#.#.#                 #.#####  ",
			"  #...#.#               YN....#.#  ",
			"  #.###.#                 #####.#  ",
			"DI....#.#                 #.....#  ",
			"  #####.#                 #.###.#  ",
			"ZZ......#               QG....#..AS",
			"  ###.###                 #######  ",
			"JO..#.#.#                 #.....#  ",
			"  #.#.#.#                 ###.#.#  ",
			"  #...#..DI             BU....#..LF",
			"  #####.#                 #.#####  ",
			"YN......#               VT..#....QG",
			"  #.###.#                 #.###.#  ",
			"  #.#...#                 #.....#  ",
			"  ###.###    J L     J    #.#.###  ",
			"  #.....#    O F     P    #.#...#  ",
			"  #.###.#####.#.#####.#####.###.#  ",
			"  #...#.#.#...#.....#.....#.#...#  ",
			"  #.#####.###.###.#.#.#########.#  ",
			"  #...#.#.....#...#.#.#.#.....#.#  ",
			"  #.###.#####.###.###.#.#.#######  ",
			"  #.#.........#...#.............#  ",
			"  #########.###.###.#############  ",
			"           B   J   C               ",
			"           U   P   P               ",
		}, -1},
		{[]string{
			"             Z L X W       C                 ",
			"             Z P Q B       K                 ",
			"  ###########.#.#.#.#######.###############  ",
			"  #...#.......#.#.......#.#.......#.#.#...#  ",
			"  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  ",
			"  #.#...#.#.#...#.#.#...#...#...#.#.......#  ",
			"  #.###.#######.###.###.#.###.###.#.#######  ",
			"  #...#.......#.#...#...#.............#...#  ",
			"  #.#########.#######.#.#######.#######.###  ",
			"  #...#.#    F       R I       Z    #.#.#.#  ",
			"  #.###.#    D       E C       H    #.#.#.#  ",
			"  #.#...#                           #...#.#  ",
			"  #.###.#                           #.###.#  ",
			"  #.#....OA                       WB..#.#..ZH",
			"  #.###.#                           #.#.#.#  ",
			"CJ......#                           #.....#  ",
			"  #######                           #######  ",
			"  #.#....CK                         #......IC",
			"  #.###.#                           #.###.#  ",
			"  #.....#                           #...#.#  ",
			"  ###.###                           #.#.#.#  ",
			"XF....#.#                         RF..#.#.#  ",
			"  #####.#                           #######  ",
			"  #......CJ                       NM..#...#  ",
			"  ###.#.#                           #.###.#  ",
			"RE....#.#                           #......RF",
			"  ###.###        X   X       L      #.#.#.#  ",
			"  #.....#        F   Q       P      #.#.#.#  ",
			"  ###.###########.###.#######.#########.###  ",
			"  #.....#...#.....#.......#...#.....#.#...#  ",
			"  #####.#.###.#######.#######.###.###.#.#.#  ",
			"  #.......#.......#.#.#.#.#...#...#...#.#.#  ",
			"  #####.###.#####.#.#.#.#.###.###.#.###.###  ",
			"  #.......#.....#.#...#...............#...#  ",
			"  #############.#.#.###.###################  ",
			"               A O F   N                     ",
			"               A A D   M                     ",
		}, 396},
	}

	for i, test := range tests {
		actual := day20.RecursiveMaze(test.in)
		if actual != test.out {
			t.Errorf("RecursiveMaze(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
