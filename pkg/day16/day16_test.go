package day16_test

import (
	"reflect"
	"testing"

	"advent-of-code-2019/pkg/day16"
)

func TestPhase(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{1, 2, 3, 4, 5, 6, 7, 8}, []int{4, 8, 2, 2, 6, 1, 5, 8}},
		{[]int{4, 8, 2, 2, 6, 1, 5, 8}, []int{3, 4, 0, 4, 0, 4, 3, 8}},
	}

	for _, test := range tests {
		actual := day16.Phase(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("Phase(%v) => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestPart1(t *testing.T) {
	tests := []struct {
		in    string
		phase int
		out   string
	}{
		{"12345678", 1, "48226158"},
		{"12345678", 2, "34040438"},
		{"12345678", 3, "03415518"},
		{"12345678", 4, "01029498"},
		{"80871224585914546619083218645595", 100, "24176176"},
		{"19617804207202209144916044189917", 100, "73745418"},
		{"69317163492948606335995924319873", 100, "52432133"},
	}

	for _, test := range tests {
		actual := day16.Part1(test.in, test.phase)
		if actual != test.out {
			t.Errorf("Part1(%q, %d) => %s, want %s", test.in, test.phase, actual, test.out)
		}
	}
}

func TestPart2(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"03036732577212944063491565474664", "84462026"},
		{"02935109699940807407585447034323", "78725270"},
		{"03081770884921959731165446850517", "53553731"},
	}

	for _, test := range tests {
		actual := day16.Part2(test.in, 100)
		if actual != test.out {
			t.Errorf("Part2(%q, 100) => %s, want %s", test.in, actual, test.out)
		}
	}
}
