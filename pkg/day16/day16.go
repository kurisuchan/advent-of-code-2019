package day16

import (
	"container/ring"
	"fmt"
	"strconv"
	"strings"
)

var basePattern = []int{0, 1, 0, -1}

func Part1(input string, phases int) string {
	var elements []int
	input = strings.TrimSpace(input)

	for i := range input {
		element, err := strconv.Atoi(input[i : i+1])
		if err == nil {
			elements = append(elements, element)
		}
	}

	for i := 0; i < phases; i++ {
		elements = Phase(elements)
	}

	var out strings.Builder
	for _, element := range elements[:8] {
		out.WriteString(strconv.Itoa(element))
	}

	return out.String()
}

func Phase(input []int) []int {
	r := ring.New(len(basePattern))
	for _, e := range basePattern {
		r.Value = e
		r = r.Next()
	}

	for _, e := range basePattern {
		r.Value = e
		r = r.Next()
	}

	result := make([]int, len(input))

	for i := 0; i < len(input); i++ {
		pos := r.Next()

		for _, element := range input {
			result[i] += element * pos.Value.(int)
			pos = pos.Next()
		}

		if result[i] < 0 {
			result[i] = -result[i]
		}

		result[i] %= 10

		// expand ring
		for _, e := range basePattern {
			n := ring.New(1)
			n.Value = e
			r = r.Link(n).Move(i)
		}
	}

	return result
}

func Part2(input string, phases int) string {
	var inputElements []int
	input = strings.TrimSpace(input)

	offset, err := strconv.Atoi(input[0:7])
	if err != nil {
		panic(fmt.Errorf("cannot convert offset %q to int: %s", input[0:7], err.Error()))
	}

	for i := range input {
		element, err := strconv.Atoi(input[i : i+1])
		if err == nil {
			inputElements = append(inputElements, element)
		}
	}

	l := len(inputElements)
	elements := make([]int, l*10000)

	for x := 0; x < 10000; x++ {
		for i := range inputElements {
			elements[x*l+i] = inputElements[i]
		}
	}

	// works only for the last half of the input
	if offset < l*10000/2 {
		panic(fmt.Errorf("optimization doesn't work for offset %d", offset))
	}


	for i := 0; i < phases; i++ {
		var sum int

		for j := l*10000-1; j >= offset; j-- {
			// value at position j is the sum of all elements from there to the end
			sum += elements[j]
			elements[j] = sum % 10
		}
	}

	var out strings.Builder
	for _, element := range elements[offset:offset+8] {
		out.WriteString(strconv.Itoa(element))
	}

	return out.String()
}
