package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day22"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Part1: %d\n", day22.Part1(input))
	fmt.Printf("Part2: %d\n", day22.Part2(input))
}
