package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day06"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")
	orbitMap := day06.ParseInput(input)

	fmt.Printf("OrbitCountChecksum: %d\n", day06.OrbitCountChecksum(orbitMap))
	fmt.Printf("OrbitalTransfers: %d\n", day06.OrbitalTransfers(orbitMap))
}
