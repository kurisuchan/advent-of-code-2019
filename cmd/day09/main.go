package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day09"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("BoostCode: %d\n", day09.BoostCode(input))
	fmt.Printf("Coordinates: %d\n", day09.Coordinates(input))
}
