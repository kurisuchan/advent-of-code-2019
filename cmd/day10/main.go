package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day10"
	"advent-of-code-2019/pkg/utils"
)

const bet = 200

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	station, visible := day10.BestLocation(input)
	fmt.Printf("BestLocation: %+v with %d\n", station, visible)
	fmt.Printf("CompleteVaporizationBet: %d\n", day10.CompleteVaporizationBet(input, station, bet))
}
