package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day04"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Part1: %d\n", day04.CountPasswords(input, day04.ValidPassword))
	fmt.Printf("Part2: %d\n", day04.CountPasswords(input, day04.AdditionallyValidPassword))
}
