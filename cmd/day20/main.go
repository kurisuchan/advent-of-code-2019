package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day20"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("DonutMaze: %d\n", day20.DonutMaze(input))
	fmt.Printf("RecursiveMaze: %d\n", day20.RecursiveMaze(input))
}
