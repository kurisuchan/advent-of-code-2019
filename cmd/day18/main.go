package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day18"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("OneVault: %d\n", day18.OneVault(input))
	fmt.Printf("QuadVault: %d\n", day18.QuadVault(input))
}
