package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day02"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")
	intcode := day02.ParseIntcode(input)

	memory := utils.CopyIntSlice(intcode)
	fmt.Printf("Part1: %d\n", day02.Part1(memory))

	memory = utils.CopyIntSlice(intcode)
	fmt.Printf("Part2: %d\n", day02.Part2(intcode))
}
