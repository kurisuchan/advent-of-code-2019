package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day25"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Adventure: %d\n", day25.Adventure(input))
}
