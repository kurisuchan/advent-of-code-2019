package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day24"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Bugs: %d\n", day24.Bugs(input))
	fmt.Printf("RecursiveBugs: %d\n", day24.RecursiveBugs(input, 200))
}
