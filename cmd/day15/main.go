package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day15"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")
	grid := day15.Explore(input)

	fmt.Printf("Map:\n%s\n", grid.String())

	fmt.Printf("FindOxygenSystem: %d steps away\n", day15.FindOxygenSystem(grid))

	t := day15.TimeToFill(grid)
	fmt.Printf("TimeToFill: %s (%.0f minutes)\n", t, t.Minutes())
}
