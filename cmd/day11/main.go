package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day11"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("AreaEstimate: %d\n", day11.AreaEstimate(input))
	fmt.Printf("RegistrationIdentifier: \n%s", day11.RegistrationIdentifier(input))
}
