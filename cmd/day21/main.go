package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day21"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("WalkHullDamage: %d\n", day21.WalkHullDamage(input))
	fmt.Printf("RunHullDamage: %d\n", day21.RunHullDamage(input))
}
