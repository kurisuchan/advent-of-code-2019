package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day13"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("CountBlocks: %d\n", day13.CountBlocks(input))
	fmt.Printf("BeatTheGame: %d\n", day13.BeatTheGame(input))
}
