package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day16"
	"advent-of-code-2019/pkg/utils"
)

const phases = 100

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Part1: %s\n", day16.Part1(input, phases))
	fmt.Printf("Part2: %s\n", day16.Part2(input, phases))
}
