package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day08"
	"advent-of-code-2019/pkg/utils"
)

const (
	width  = 25
	height = 6
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("ImageChecksum: %d\n", day08.ImageChecksum(input, width, height))
	fmt.Printf("ImageRender:\n%s\n", day08.ImageRender(input, width, height))
}
