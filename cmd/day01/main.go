package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day01"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	fmt.Printf("ModuleMass:         %d\n", day01.ModuleMass(input))
	fmt.Printf("AdjustedModuleMass: %d\n", day01.AdjustedModuleMass(input))
}
