package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day23"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Part1: %d\n", day23.Part1(input))
	fmt.Printf("Part2: %d\n", day23.Part2(input))
}
