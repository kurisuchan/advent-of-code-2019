package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day12"
	"advent-of-code-2019/pkg/utils"
)

const steps = 1000

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Part1: %d\n", day12.Part1(input, steps))
	fmt.Printf("Part2: %d\n", day12.Part2(input))
}
