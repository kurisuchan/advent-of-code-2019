package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day05"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("AirConditioner: %d\n", day05.AirConditioner(input))
	fmt.Printf("ThermalRadiators: %d\n", day05.ThermalRadiators(input))
}
