package main

import (
	"fmt"

	"advent-of-code-2019/pkg/day14"
	"advent-of-code-2019/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("OreForFuel: %d\n", day14.OreForFuel(input))
	fmt.Printf("FuelForOre: %d\n", day14.FuelForOre(input))
}
